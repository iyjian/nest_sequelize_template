export type MysqlConfig = {
  host: string
  port: number
  username: string
  password: string
  db?: string
}

export type RedisConfig = {
  host: string
  port: number
  password?: string
  db?: number
  ttl: number
}

export type RabbitConfig = {
  host: string
  port: number
  user: string
  passwd: string
  uri: string
}

export type AppConfig = {
  code: string
  title: string
  desc: string
  port: number
  env: 'development' | 'production'
}

export type AuthServerConfig = {
  server: string
}

export type ExchangeConfig = {
  exchange: string
  queue: string
  dlx: string
  dlQueue: string
}

export type MeiliSearch = {
  endpoint: string
  masterKey: string
}

export type MINIO = {
  accessKey: string
  secretKey: string
  server: string
  port: number
  publicEndpoint: string
  bucket: string
  thumbnailBucket: string
  publicBucket: string
  publicThumbnailBucket: string
}

export type MONGO = {
  host: string
  port: number
  loggingDB: string
  loggingDBURI: string
}

export type DEVTOOL = {
  endpoint: string
  token: string
}

export type ConfigType = {
  app: AppConfig
  sqlLogging: string
  mysql: MysqlConfig
  redis: RedisConfig
  auth: AuthServerConfig
  rabbit: RabbitConfig
  loggingExchange: ExchangeConfig
  asyncTaskExchange: ExchangeConfig
  meiliSearch: MeiliSearch
  minio: MINIO
  mongo: MONGO
  devtool: DEVTOOL
}
