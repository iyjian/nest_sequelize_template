type CUSTOME_ERROR_TYPE = { [errorCode: string]: [string, number] }

export const CUSTOME_ERROR: CUSTOME_ERROR_TYPE = {
  CREW_SHIFT_NOT_READY: ['未找到排班计划', 200],
}
