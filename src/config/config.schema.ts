import Joi from 'joi'

export const configSchema = Joi.object({
  APP_CODE: Joi.string(),
  APP_TITLE: Joi.string(),
  APP_DESC: Joi.string(),
  PORT: Joi.number().integer().min(1000).max(65535).required(),

  NODE_ENV: Joi.string().valid('development', 'production', 'test').required(),

  AUTH_SERVER: Joi.string().required(),

  SQL_LOGGING: Joi.string().optional(),

  // REDIS
  REDIS_HOST: Joi.string().required(),
  REDIS_PORT: Joi.string().required(),
  REDIS_PASSWD: Joi.optional(),
  REDIS_DB: Joi.string().required(),
  REDIS_TTL: Joi.string().optional(),

  // MYSQL
  MYSQL_HOST: Joi.string().required(),
  MYSQL_PORT: Joi.string().required(),
  MYSQL_USER: Joi.string().required(),
  MYSQL_PASSWD: Joi.string().required(),
  MYSQL_DB: Joi.string().required(),

  // LOCALRABBIT
  RABBITMQ_USER: Joi.string().required(),
  RABBITMQ_PASSWD: Joi.string().required(),
  RABBITMQ_HOST: Joi.string().required(),
  RABBITMQ_PORT: Joi.string().required(),

  // 日志记录队列配置
  LOGGING_BASE_NAME: Joi.string().required(),

  // 异步任务队列配置
  ASYNC_TASK_BASE_NAME: Joi.string().required(),

  // MAIL CONFIG
  // MAIL_SMTP_SERVER: Joi.string().required(),
  // MAIL_PORT: Joi.string().required(),
  // MAIL_SEND_FROM: Joi.string().required(),
  // MAIL_USER_NAME: Joi.string().required(),
  // MAIL_PASSWORD: Joi.string().required(),

  MEILISEARCH_ENDPOINT: Joi.string().required(),
  MEILISEARCH_MASTER_KEY: Joi.string().required(),

  // MINIO
  MINIO_SECRET_KEY: Joi.string().required(),
  MINIO_ACCESS_KEY: Joi.string().required(),
  MINIO_SERVER: Joi.string().required(),
  MINIO_PORT: Joi.string().required(),
  MINIO_BUCKET: Joi.string().required(),
  MINIO_THUMBNAIL_BUCKET: Joi.string().required(),
  MINIO_PUBLIC_ENDPOINT: Joi.string().required(),
})
