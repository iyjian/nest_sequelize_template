import { ConfigType, ExchangeConfig } from './types'

/**
 * 每种
 *
 * @param baseName - 基础命名
 * @returns
 */
const getExchangeConfig = (baseName: string): ExchangeConfig => {
  return {
    exchange: `ex.${baseName}`,
    queue: `q.${baseName}`,
    dlx: `dlx.${baseName}`,
    dlQueue: `q.dl.${baseName}`,
  }
}

export default (): ConfigType => ({
  sqlLogging: process.env.SQL_LOGGING,

  mysql: {
    host: process.env.MYSQL_HOST,
    port: parseInt(process.env.MYSQL_PORT, 0),
    username: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASSWD,
    db: process.env.MYSQL_DB,
  },

  redis: {
    host: process.env.REDIS_HOST,
    port: parseInt(process.env.REDIS_PORT, 0),
    password: process.env.REDIS_PASSWD,
    db: parseInt(process.env.REDIS_DB, 0),
    ttl: parseInt(process.env.REDIS_TTL, 0),
  },
  auth: {
    server: process.env.AUTH_SERVER,
  },
  app: {
    code: process.env.APP_CODE,
    title: process.env.APP_TITLE,
    desc: process.env.APP_DESC,
    port: parseInt(process.env.PORT),
    env:
      process.env.NODE_ENV === 'development'
        ? 'development'
        : process.env.NODE_ENV === 'production'
        ? 'production'
        : undefined,
  },
  rabbit: {
    user: process.env.RABBITMQ_USER,
    passwd: process.env.RABBITMQ_PASSWD,
    host: process.env.RABBITMQ_HOST,
    port: parseInt(process.env.RABBITMQ_PORT, 0),
    uri: `amqp://${process.env.RABBITMQ_USER}:${process.env.RABBITMQ_PASSWD}@${process.env.RABBITMQ_HOST}:${process.env.RABBITMQ_PORT}`,
  },
  meiliSearch: {
    endpoint: process.env.MEILISEARCH_ENDPOINT,
    masterKey: process.env.MEILISEARCH_MASTER_KEY,
  },
  minio: {
    accessKey: process.env.MINIO_ACCESS_KEY,
    secretKey: process.env.MINIO_SECRET_KEY,
    server: process.env.MINIO_SERVER,
    port: parseInt(process.env.MINIO_PORT),
    publicEndpoint: process.env.MINIO_PUBLIC_ENDPOINT,
    bucket: process.env.MINIO_BUCKET,
    thumbnailBucket: process.env.MINIO_THUMBNAIL_BUCKET,
    publicBucket: process.env.MINIO_PUBLIC_BUCKET,
    publicThumbnailBucket: process.env.MINIO_PUBLIC_THUMBNAIL_BUCKET,
  },
  mongo: {
    host: process.env.MONGO_HOST,
    port: parseInt(process.env.MONGO_PORT),
    loggingDB: process.env.MONGO_LOGGING_DB,
    loggingDBURI: `mongodb://${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/${process.env.MONGO_LOGGING_DB}`,
  },
  devtool: {
    endpoint: process.env.DEVTOOL_ENDPOINT,
    token: process.env.DEVTOOL_TOKEN,
  },
  loggingExchange: getExchangeConfig(process.env.LOGGING_BASE_NAME),
  asyncTaskExchange: getExchangeConfig(process.env.ASYNC_TASK_BASE_NAME),
})
