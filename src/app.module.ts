import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common'
import { CacheModule } from '@nestjs/cache-manager'
import { AuthMiddleware } from './core'
import configuration from './config/configuration'
import redisStore from 'cache-manager-ioredis'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { SequelizeModule } from '@nestjs/sequelize'
import { configSchema } from './config/config.schema'
import { AsyncClientModule } from './features/async-client'
import { EnumModule } from './features/enum'
import { BaseModule } from './features/base'
import { AttachmentModule } from './features/attachment/attachment.module'

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [configuration],
      isGlobal: true,
      validationSchema: configSchema,
      validationOptions: {
        allowUnknown: true,
        abortEarly: true,
      },
    }),
    SequelizeModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        dialect: 'mysql',
        host: configService.get('mysql.host'),
        port: +configService.get<number>('mysql.port'),
        username: configService.get('mysql.username'),
        password: configService.get('mysql.password'),
        database: configService.get('mysql.db'),
        models: [],
        autoLoadModels: true,
        synchronize: true,
        pool: {
          max: 100,
          min: 0,
          idle: 10000,
        },
        timezone: '+08:00',
        logging:
          configService.get<string>('sqlLogging') === 'true' ? true : false,
      }),
      inject: [ConfigService],
    }),
    CacheModule.registerAsync({
      useFactory: (configService: ConfigService) => ({
        store: redisStore,
        host: configService.get('redis.host'),
        port: +configService.get<number>('redis.port'),
        password: configService.get('redis.password'),
        db: +configService.get<number>('redis.db'),
        ttl: 0,
      }),
      inject: [ConfigService],
      isGlobal: true,
    }),
    BaseModule,
    EnumModule,
    AsyncClientModule,
    AttachmentModule,
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthMiddleware)
      .forRoutes({ path: '*', method: RequestMethod.ALL })
  }
}
