import { NestFactory } from '@nestjs/core'
import { NestExpressApplication } from '@nestjs/platform-express'
import express from 'express'
import morgan from 'morgan'
import helmet from 'helmet'
import compression from 'compression'
import { AppModule } from './app.module'
import { ConfigService } from '@nestjs/config'
import { Logger } from '@nestjs/common'
import {
  ValidationPipeWithTransform,
  AllExceptionFilter,
  LoggingInterceptor,
  TransformInterceptor,
} from './core'
// 如果用pkg打包，则需要显示的引用mysql2,pkg才会将mysql2打在包里，因为sequelize引用mysql2的方式是动态的，pkg无法检测出mysql2是否需要引入
import 'mysql2'
import { AsyncClientService } from './features/async-client'
import { AppConfig } from './config'
import setupSwagger from './scripts/setupSwagger'
import logRoutes from './scripts/logRoutes'

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule)
  const configService = app.get(ConfigService)
  const appConfig = configService.get<AppConfig>('app')

  app.useGlobalPipes(new ValidationPipeWithTransform())
  app.enableCors()
  app.set('trust proxy', true)
  app.set('etag', false)
  app.use(express.json({ limit: '50mb' }))
  app.use(express.urlencoded({ limit: '50mb', extended: true }))
  app.use(compression())
  app.use(morgan('combined'))
  app.use(helmet())
  app.useGlobalFilters(new AllExceptionFilter())
  app.useGlobalInterceptors(new TransformInterceptor())
  app.useGlobalInterceptors(new LoggingInterceptor(app.get(AsyncClientService)))

  /**
   * nest swagger config(enabled on development env)
   */
  if (appConfig.env === 'development') {
    setupSwagger(app)
  }

  await app.listen(appConfig.port)

  logRoutes(app)

  const logger = new Logger('NestApplication')

  logger.debug(`application started at port: ${appConfig.port}`)
}
bootstrap()
