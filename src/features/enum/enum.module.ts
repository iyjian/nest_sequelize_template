import { Module } from '@nestjs/common'
import {
  EnumCategoryController,
  EnumTypeController,
  EnumController,
} from './controllers'
import { EnumCategoryService, EnumTypeService, EnumService } from './services'
import { EnumCategory, EnumType, Enum } from './entities'
import { SequelizeModule } from '@nestjs/sequelize'

@Module({
  controllers: [EnumCategoryController, EnumTypeController, EnumController],
  providers: [EnumCategoryService, EnumTypeService, EnumService],
  imports: [SequelizeModule.forFeature([EnumCategory, EnumType, Enum])],
  exports: [EnumCategoryService, EnumTypeService, EnumService],
})
export class EnumModule {}
