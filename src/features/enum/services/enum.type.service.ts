import { Injectable, HttpException, HttpStatus, Logger } from '@nestjs/common'
import { InjectModel } from '@nestjs/sequelize'
import { Op, Transaction } from 'sequelize'
import { Sequelize } from 'sequelize-typescript'
import { BaseService } from './../../../core'
import { EnumType } from './../entities'
import {
  FindAllEnumTypeRequestDTO,
  FindOneEnumTypeRequestDTO,
  CreateEnumTypeRequestDTO,
  UpdateEnumTypeRequestDTO,
} from './../dto'

@Injectable()
export class EnumTypeService extends BaseService {
  private readonly include: any[]

  constructor(
    @InjectModel(EnumType)
    private readonly enumTypeModel: typeof EnumType,
    private readonly mysql: Sequelize,
  ) {
    super()
    this.include = []
  }

  async create(
    createEnumTypeRequest: CreateEnumTypeRequestDTO,
    transaction?: Transaction,
  ) {
    const isOuterTransaction = !!transaction
    try {
      if (!isOuterTransaction) {
        transaction = await this.mysql.transaction()
      }

      const enumType = await this.enumTypeModel.create(createEnumTypeRequest, {
        transaction,
      })

      if (!isOuterTransaction) {
        await transaction.commit()
      }

      return this.findOneById(enumType.id)
    } catch (e) {
      if (!isOuterTransaction && transaction) {
        await transaction.rollback()
      }
      throw e
    }
  }

  async findAll(
    findAllEnumTypeRequest: FindAllEnumTypeRequestDTO,
    transaction?: Transaction,
  ) {
    const {
      page = 1,
      pageSize = 20,
      skipPaging,
      search,
      sort = [['id', 'desc']],
      ...payload
    } = findAllEnumTypeRequest

    const condition = this.normalizeCondition(payload)

    if (search) {
      condition[Op.or] = {}
    }

    const enumTypes = await this.enumTypeModel
      .scope('findAll')
      .findAndCountAll({
        where: { ...condition },
        offset: skipPaging ? undefined : (page - 1) * pageSize,
        limit: skipPaging ? undefined : pageSize,
        order: sort,
        transaction,
      })

    return enumTypes
  }

  async findByIds(ids: number[], transaction?: Transaction) {
    const enumTypes = await this.enumTypeModel.findAll({
      where: {
        id: {
          [Op.in]: ids,
        },
      },
      transaction,
    })

    return enumTypes
  }

  async findOneByIdOrThrow(id: number, transaction?: Transaction) {
    const enumType = await this.enumTypeModel.scope('findOne').findByPk(id, {
      transaction,
    })

    if (!enumType) {
      throw new HttpException('未找到该记录', HttpStatus.NOT_FOUND)
    }

    return enumType
  }

  async findOneById(id: number, transaction?: Transaction) {
    const enumType = await this.enumTypeModel.scope('findOne').findByPk(id, {
      transaction,
    })

    return enumType
  }

  async findOneOrThrow(
    findOneEnumTypeRequest: FindOneEnumTypeRequestDTO,
    transaction?: Transaction,
  ) {
    const enumType = await this.enumTypeModel.scope('findOne').findOne({
      where: { ...findOneEnumTypeRequest },
      transaction,
    })

    if (!enumType) {
      throw new HttpException('未找到该记录', HttpStatus.NOT_FOUND)
    }

    return enumType
  }

  async findOne(
    findOneEnumTypeRequest: FindOneEnumTypeRequestDTO,
    transaction?: Transaction,
  ) {
    const enumType = await this.enumTypeModel.scope('findOne').findOne({
      where: { ...findOneEnumTypeRequest },
      transaction,
    })

    return enumType
  }

  async updateById(
    id: number,
    updateEnumTypeRequest: UpdateEnumTypeRequestDTO,
    transaction?: Transaction,
  ) {
    await this.enumTypeModel.update(updateEnumTypeRequest, {
      where: {
        id,
      },
      transaction,
    })
    return true
  }

  async removeById(id: number, transaction?: Transaction) {
    const isOuterTransaction = !!transaction
    try {
      if (!transaction) {
        transaction = await this.mysql.transaction()
      }

      await this.enumTypeModel.update(
        { isActive: null },
        {
          where: {
            id,
          },
          transaction,
        },
      )

      if (!isOuterTransaction) {
        await transaction.commit()
      }
      return true
    } catch (e) {
      if (transaction && !isOuterTransaction) {
        await transaction.rollback()
        if (e.toString() === 'Error: SequelizeForeignKeyConstraintError') {
          throw new Error('有依赖数据，无法删除')
        }
      }
      throw e
    }
  }
}
