import { Injectable, HttpException, HttpStatus, Logger } from '@nestjs/common'
import { InjectModel } from '@nestjs/sequelize'
import { Op, Transaction } from 'sequelize'
import { Sequelize } from 'sequelize-typescript'
import { BaseService } from './../../../core'
import { EnumCategory } from './../entities'
import {
  FindAllEnumCategoryRequestDTO,
  FindOneEnumCategoryRequestDTO,
  CreateEnumCategoryRequestDTO,
  UpdateEnumCategoryRequestDTO,
} from './../dto'

@Injectable()
export class EnumCategoryService extends BaseService {
  private readonly include: any[]

  constructor(
    @InjectModel(EnumCategory)
    private readonly enumCategoryModel: typeof EnumCategory,
    private readonly mysql: Sequelize,
  ) {
    super()
    this.include = []
  }

  async create(
    createEnumCategoryRequest: CreateEnumCategoryRequestDTO,
    transaction?: Transaction,
  ) {
    const isOuterTransaction = !!transaction
    try {
      if (!isOuterTransaction) {
        transaction = await this.mysql.transaction()
      }

      const enumCategory = await this.enumCategoryModel.create(
        createEnumCategoryRequest,
        { transaction },
      )

      if (!isOuterTransaction) {
        await transaction.commit()
      }

      return this.findOneById(enumCategory.id)
    } catch (e) {
      if (!isOuterTransaction && transaction) {
        await transaction.rollback()
      }
      throw e
    }
  }

  async findAll(
    findAllEnumCategoryRequest: FindAllEnumCategoryRequestDTO,
    transaction?: Transaction,
  ) {
    const {
      page = 1,
      pageSize = 20,
      skipPaging,
      search,
      sort = [['id', 'desc']],
      ...payload
    } = findAllEnumCategoryRequest

    const condition = this.normalizeCondition(payload)

    if (search) {
      condition[Op.or] = {}
    }

    const enumCategories = await this.enumCategoryModel
      .scope('findAll')
      .findAndCountAll({
        where: { ...condition },
        offset: skipPaging ? undefined : (page - 1) * pageSize,
        limit: skipPaging ? undefined : pageSize,
        order: sort,
        transaction,
      })

    return enumCategories
  }

  async findByIds(ids: number[], transaction?: Transaction) {
    const enumCategories = await this.enumCategoryModel.findAll({
      where: {
        id: {
          [Op.in]: ids,
        },
      },
      transaction,
    })

    return enumCategories
  }

  async findOneByIdOrThrow(id: number, transaction?: Transaction) {
    const enumCategory = await this.enumCategoryModel
      .scope('findOne')
      .findByPk(id, {
        transaction,
      })

    if (!enumCategory) {
      throw new HttpException('未找到该记录', HttpStatus.NOT_FOUND)
    }

    return enumCategory
  }

  async findOneById(id: number, transaction?: Transaction) {
    const enumCategory = await this.enumCategoryModel
      .scope('findOne')
      .findByPk(id, {
        transaction,
      })

    return enumCategory
  }

  async findOneOrThrow(
    findOneEnumCategoryRequest: FindOneEnumCategoryRequestDTO,
    transaction?: Transaction,
  ) {
    const enumCategory = await this.enumCategoryModel.scope('findOne').findOne({
      where: { ...findOneEnumCategoryRequest },
      transaction,
    })

    if (!enumCategory) {
      throw new HttpException('未找到该记录', HttpStatus.NOT_FOUND)
    }

    return enumCategory
  }

  async findOne(
    findOneEnumCategoryRequest: FindOneEnumCategoryRequestDTO,
    transaction?: Transaction,
  ) {
    const enumCategory = await this.enumCategoryModel.scope('findOne').findOne({
      where: { ...findOneEnumCategoryRequest },
      transaction,
    })

    return enumCategory
  }

  async updateById(
    id: number,
    updateEnumCategoryRequest: UpdateEnumCategoryRequestDTO,
    transaction?: Transaction,
  ) {
    await this.enumCategoryModel.update(updateEnumCategoryRequest, {
      where: {
        id,
      },
      transaction,
    })
    return true
  }

  async removeById(id: number, transaction?: Transaction) {
    const isOuterTransaction = !!transaction
    try {
      if (!transaction) {
        transaction = await this.mysql.transaction()
      }

      await this.enumCategoryModel.update(
        { isActive: null },
        {
          where: {
            id,
          },
          transaction,
        },
      )

      if (!isOuterTransaction) {
        await transaction.commit()
      }
      return true
    } catch (e) {
      if (transaction && !isOuterTransaction) {
        await transaction.rollback()
        if (e.toString() === 'Error: SequelizeForeignKeyConstraintError') {
          throw new Error('有依赖数据，无法删除')
        }
      }
      throw e
    }
  }
}
