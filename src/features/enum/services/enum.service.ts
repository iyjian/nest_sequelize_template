import { Injectable, HttpException, HttpStatus, Logger } from '@nestjs/common'
import { InjectModel } from '@nestjs/sequelize'
import { Op, Transaction } from 'sequelize'
import { Sequelize } from 'sequelize-typescript'
import { BaseService } from './../../../core'
import { Enum } from './../entities'
import {
  FindAllEnumRequestDTO,
  FindOneEnumRequestDTO,
  CreateEnumRequestDTO,
  UpdateEnumRequestDTO,
} from './../dto'

@Injectable()
export class EnumService extends BaseService {
  private readonly include: any[]

  constructor(
    @InjectModel(Enum)
    private readonly enum_Model: typeof Enum,
    private readonly mysql: Sequelize,
  ) {
    super()
    this.include = []
  }

  async create(
    createEnumRequest: CreateEnumRequestDTO,
    transaction?: Transaction,
  ) {
    const isOuterTransaction = !!transaction
    try {
      if (!isOuterTransaction) {
        transaction = await this.mysql.transaction()
      }

      const enum_ = await this.enum_Model.create(createEnumRequest, {
        transaction,
      })

      if (!isOuterTransaction) {
        await transaction.commit()
      }

      return this.findOneById(enum_.id)
    } catch (e) {
      if (!isOuterTransaction && transaction) {
        await transaction.rollback()
      }
      throw e
    }
  }

  async findAll(
    findAllEnumRequest: FindAllEnumRequestDTO,
    transaction?: Transaction,
  ) {
    const {
      page = 1,
      pageSize = 20,
      skipPaging,
      search,
      sort = [['id', 'desc']],
      ...payload
    } = findAllEnumRequest

    const condition = this.normalizeCondition(payload)

    if (search) {
      condition[Op.or] = {}
    }

    const enum_s = await this.enum_Model.scope('findAll').findAndCountAll({
      where: { ...condition },
      offset: skipPaging ? undefined : (page - 1) * pageSize,
      limit: skipPaging ? undefined : pageSize,
      order: sort,
      transaction,
    })

    return enum_s
  }

  async findByIds(ids: number[], transaction?: Transaction) {
    const enum_s = await this.enum_Model.findAll({
      where: {
        id: {
          [Op.in]: ids,
        },
      },
      transaction,
    })

    return enum_s
  }

  async findOneByIdOrThrow(id: number, transaction?: Transaction) {
    const enum_ = await this.enum_Model.scope('findOne').findByPk(id, {
      transaction,
    })

    if (!enum_) {
      throw new HttpException('未找到该记录', HttpStatus.NOT_FOUND)
    }

    return enum_
  }

  async findOneById(id: number, transaction?: Transaction) {
    const enum_ = await this.enum_Model.scope('findOne').findByPk(id, {
      transaction,
    })

    return enum_
  }

  async findOneOrThrow(
    findOneEnumRequest: FindOneEnumRequestDTO,
    transaction?: Transaction,
  ) {
    const enum_ = await this.enum_Model.scope('findOne').findOne({
      where: { ...findOneEnumRequest },
      transaction,
    })

    if (!enum_) {
      throw new HttpException('未找到该记录', HttpStatus.NOT_FOUND)
    }

    return enum_
  }

  async findOne(
    findOneEnumRequest: FindOneEnumRequestDTO,
    transaction?: Transaction,
  ) {
    const enum_ = await this.enum_Model.scope('findOne').findOne({
      where: { ...findOneEnumRequest },
      transaction,
    })

    return enum_
  }

  async updateById(
    id: number,
    updateEnumRequest: UpdateEnumRequestDTO,
    transaction?: Transaction,
  ) {
    await this.enum_Model.update(updateEnumRequest, {
      where: {
        id,
      },
      transaction,
    })
    return true
  }

  async removeById(id: number, transaction?: Transaction) {
    const isOuterTransaction = !!transaction
    try {
      if (!transaction) {
        transaction = await this.mysql.transaction()
      }

      await this.enum_Model.update(
        { isActive: null },
        {
          where: {
            id,
          },
          transaction,
        },
      )

      if (!isOuterTransaction) {
        await transaction.commit()
      }
      return true
    } catch (e) {
      if (transaction && !isOuterTransaction) {
        await transaction.rollback()
        if (e.toString() === 'Error: SequelizeForeignKeyConstraintError') {
          throw new Error('有依赖数据，无法删除')
        }
      }
      throw e
    }
  }
}
