export const FindOneResponseSchema = {
  enumTypeId: { type: 'number', example: '', description: '枚举类型id' },
  cnName: { type: 'string', example: '', description: '中文名字' },
  enName: { type: 'string', example: '', description: '英文名字' },
  code: { type: 'string', example: '', description: '代码' },
  remark: { type: 'string', example: '', description: '枚举值备注' },
  status: { type: 'boolean', example: '', description: '状态' },
  num: {
    type: 'number',
    example: '',
    description: '枚举值类型下的具体枚举值的编号',
  },
  order: { type: 'number', example: '', description: '排序' },
  isSystem: { type: 'boolean', example: '', description: '是否系统保留' },
}

export const FindAllResponseSchema = {
  enumTypeId: { type: 'number', example: '', description: '枚举类型id' },
  cnName: { type: 'string', example: '', description: '中文名字' },
  enName: { type: 'string', example: '', description: '英文名字' },
  code: { type: 'string', example: '', description: '代码' },
  remark: { type: 'string', example: '', description: '枚举值备注' },
  status: { type: 'boolean', example: '', description: '状态' },
  num: {
    type: 'number',
    example: '',
    description: '枚举值类型下的具体枚举值的编号',
  },
  order: { type: 'number', example: '', description: '排序' },
  isSystem: { type: 'boolean', example: '', description: '是否系统保留' },
}
