import { PagingRequestDTO, getTransformer, codeGen } from './../../../core'
import { Transform, Type } from 'class-transformer'
import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty } from 'class-validator'

export class CreateEnumCategoryRequestDTO {
  @codeGen('7766')
  @ApiProperty({
    description: '分类中文',
    required: false,
  })
  cnName?: string

  @codeGen('7767')
  @ApiProperty({
    description: '分类英文',
    required: false,
  })
  enName?: string

  @codeGen('7768')
  @ApiProperty({
    description: 'icon的css',
    required: false,
  })
  icon?: string

  @codeGen('7769')
  @ApiProperty({
    description: '中文描述',
    required: false,
  })
  cnDesc?: string

  @codeGen('7770')
  @ApiProperty({
    description: '英文描述',
    required: false,
  })
  enDesc?: string

  @codeGen('7771')
  @ApiProperty({
    description: '排序',
    required: false,
  })
  @Transform(getTransformer('numberTransformer'))
  order?: number
}

export class UpdateEnumCategoryRequestDTO {
  @codeGen('7766')
  @ApiProperty({
    description: '分类中文',
    required: false,
  })
  cnName?: string

  @codeGen('7767')
  @ApiProperty({
    description: '分类英文',
    required: false,
  })
  enName?: string

  @codeGen('7768')
  @ApiProperty({
    description: 'icon的css',
    required: false,
  })
  icon?: string

  @codeGen('7769')
  @ApiProperty({
    description: '中文描述',
    required: false,
  })
  cnDesc?: string

  @codeGen('7770')
  @ApiProperty({
    description: '英文描述',
    required: false,
  })
  enDesc?: string

  @codeGen('7771')
  @ApiProperty({
    description: '排序',
    required: false,
  })
  @Transform(getTransformer('numberTransformer'))
  order?: number
}

export class FindOneEnumCategoryRequestDTO {
  @codeGen('7766')
  @ApiProperty({
    description: '分类中文',
    required: false,
  })
  cnName?: string

  @codeGen('7767')
  @ApiProperty({
    description: '分类英文',
    required: false,
  })
  enName?: string

  @codeGen('7768')
  @ApiProperty({
    description: 'icon的css',
    required: false,
  })
  icon?: string

  @codeGen('7769')
  @ApiProperty({
    description: '中文描述',
    required: false,
  })
  cnDesc?: string

  @codeGen('7770')
  @ApiProperty({
    description: '英文描述',
    required: false,
  })
  enDesc?: string

  @codeGen('7771')
  @ApiProperty({
    description: '排序',
    required: false,
  })
  @Transform(getTransformer('numberTransformer'))
  order?: number
}

export class FindAllEnumCategoryRequestDTO extends PagingRequestDTO {
  @codeGen('7766')
  @ApiProperty({
    description: '分类中文',
    required: false,
  })
  cnName?: string

  @codeGen('7767')
  @ApiProperty({
    description: '分类英文',
    required: false,
  })
  enName?: string

  @codeGen('7768')
  @ApiProperty({
    description: 'icon的css',
    required: false,
  })
  icon?: string

  @codeGen('7769')
  @ApiProperty({
    description: '中文描述',
    required: false,
  })
  cnDesc?: string

  @codeGen('7770')
  @ApiProperty({
    description: '英文描述',
    required: false,
  })
  enDesc?: string

  @codeGen('7771')
  @ApiProperty({
    description: '排序',
    required: false,
  })
  @Transform(getTransformer('numberTransformer'))
  order?: number
}
