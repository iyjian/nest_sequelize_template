import { PagingRequestDTO, getTransformer, codeGen } from './../../../core'
import { Transform, Type } from 'class-transformer'
import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty } from 'class-validator'

export class CreateEnumTypeRequestDTO {
  @codeGen('7760')
  @ApiProperty({
    description: '类型代码',
    required: true,
  })
  @IsNotEmpty()
  type: string

  @codeGen('7761')
  @ApiProperty({
    description: '类型中文名',
    required: true,
  })
  @IsNotEmpty()
  cnName: string

  @codeGen('7762')
  @ApiProperty({
    description: '类型英文名',
    required: false,
  })
  enName?: string

  @codeGen('7763')
  @ApiProperty({
    description: '排序值',
    required: true,
  })
  @IsNotEmpty()
  @Transform(getTransformer('numberTransformer'))
  order: number

  @codeGen('7764')
  @ApiProperty({
    description: '枚举分类id',
    required: true,
  })
  @IsNotEmpty()
  @Transform(getTransformer('numberTransformer'))
  categoryId: number

  @codeGen('7765')
  @ApiProperty({
    description: '是否系统保留',
    required: false,
  })
  @Transform(getTransformer('booleanTransformer'))
  isSystem?: boolean
}

export class UpdateEnumTypeRequestDTO {
  @codeGen('7760')
  @ApiProperty({
    description: '类型代码',
    required: false,
  })
  type?: string

  @codeGen('7761')
  @ApiProperty({
    description: '类型中文名',
    required: false,
  })
  cnName?: string

  @codeGen('7762')
  @ApiProperty({
    description: '类型英文名',
    required: false,
  })
  enName?: string

  @codeGen('7763')
  @ApiProperty({
    description: '排序值',
    required: false,
  })
  @Transform(getTransformer('numberTransformer'))
  order?: number

  @codeGen('7764')
  @ApiProperty({
    description: '枚举分类id',
    required: false,
  })
  @Transform(getTransformer('numberTransformer'))
  categoryId?: number

  @codeGen('7765')
  @ApiProperty({
    description: '是否系统保留',
    required: false,
  })
  @Transform(getTransformer('booleanTransformer'))
  isSystem?: boolean
}

export class FindOneEnumTypeRequestDTO {
  @codeGen('7760')
  @ApiProperty({
    description: '类型代码',
    required: false,
  })
  type?: string

  @codeGen('7761')
  @ApiProperty({
    description: '类型中文名',
    required: false,
  })
  cnName?: string

  @codeGen('7762')
  @ApiProperty({
    description: '类型英文名',
    required: false,
  })
  enName?: string

  @codeGen('7763')
  @ApiProperty({
    description: '排序值',
    required: false,
  })
  @Transform(getTransformer('numberTransformer'))
  order?: number

  @codeGen('7764')
  @ApiProperty({
    description: '枚举分类id',
    required: false,
  })
  @Transform(getTransformer('numberTransformer'))
  categoryId?: number

  @codeGen('7765')
  @ApiProperty({
    description: '是否系统保留',
    required: false,
  })
  @Transform(getTransformer('booleanTransformer'))
  isSystem?: boolean
}

export class FindAllEnumTypeRequestDTO extends PagingRequestDTO {
  @codeGen('7760')
  @ApiProperty({
    description: '类型代码',
    required: false,
  })
  type?: string

  @codeGen('7761')
  @ApiProperty({
    description: '类型中文名',
    required: false,
  })
  cnName?: string

  @codeGen('7762')
  @ApiProperty({
    description: '类型英文名',
    required: false,
  })
  enName?: string

  @codeGen('7763')
  @ApiProperty({
    description: '排序值',
    required: false,
  })
  @Transform(getTransformer('numberTransformer'))
  order?: number

  @codeGen('7764')
  @ApiProperty({
    description: '枚举分类id',
    required: false,
  })
  @Transform(getTransformer('numberTransformer'))
  categoryId?: number

  @codeGen('7765')
  @ApiProperty({
    description: '是否系统保留',
    required: false,
  })
  @Transform(getTransformer('booleanTransformer'))
  isSystem?: boolean
}
