export const FindOneResponseSchema = {
  type: { type: 'string', example: '', description: '类型代码' },
  cnName: { type: 'string', example: '', description: '类型中文名' },
  enName: { type: 'string', example: '', description: '类型英文名' },
  order: { type: 'number', example: '', description: '排序值' },
  categoryId: { type: 'number', example: '', description: '枚举分类id' },
  isSystem: { type: 'boolean', example: '', description: '是否系统保留' },
}

export const FindAllResponseSchema = {
  type: { type: 'string', example: '', description: '类型代码' },
  cnName: { type: 'string', example: '', description: '类型中文名' },
  enName: { type: 'string', example: '', description: '类型英文名' },
  order: { type: 'number', example: '', description: '排序值' },
  categoryId: { type: 'number', example: '', description: '枚举分类id' },
  isSystem: { type: 'boolean', example: '', description: '是否系统保留' },
}
