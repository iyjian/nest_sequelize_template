export const FindOneResponseSchema = {
  cnName: { type: 'string', example: '', description: '分类中文' },
  enName: { type: 'string', example: '', description: '分类英文' },
  icon: { type: 'string', example: '', description: 'icon的css' },
  cnDesc: { type: 'string', example: '', description: '中文描述' },
  enDesc: { type: 'string', example: '', description: '英文描述' },
  order: { type: 'number', example: '', description: '排序' },
}

export const FindAllResponseSchema = {
  cnName: { type: 'string', example: '', description: '分类中文' },
  enName: { type: 'string', example: '', description: '分类英文' },
  icon: { type: 'string', example: '', description: 'icon的css' },
  cnDesc: { type: 'string', example: '', description: '中文描述' },
  enDesc: { type: 'string', example: '', description: '英文描述' },
  order: { type: 'number', example: '', description: '排序' },
}
