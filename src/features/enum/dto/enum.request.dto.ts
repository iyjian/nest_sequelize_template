import { PagingRequestDTO, getTransformer, codeGen } from './../../../core'
import { Transform, Type } from 'class-transformer'
import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty } from 'class-validator'

export class CreateEnumRequestDTO {
  @codeGen('7751')
  @ApiProperty({
    description: '枚举类型id',
    required: false,
  })
  @Transform(getTransformer('numberTransformer'))
  enumTypeId?: number

  @codeGen('7752')
  @ApiProperty({
    description: '中文名字',
    required: true,
  })
  @IsNotEmpty()
  cnName: string

  @codeGen('7753')
  @ApiProperty({
    description: '英文名字',
    required: false,
  })
  enName?: string

  @codeGen('7754')
  @ApiProperty({
    description: '代码',
    required: false,
  })
  code?: string

  @codeGen('7755')
  @ApiProperty({
    description: '枚举值备注',
    required: false,
  })
  remark?: string

  @codeGen('7756')
  @ApiProperty({
    description: '状态',
    required: true,
  })
  @IsNotEmpty()
  @Transform(getTransformer('booleanTransformer'))
  status: boolean

  @codeGen('7757')
  @ApiProperty({
    description: '枚举值类型下的具体枚举值的编号',
    required: false,
  })
  @Transform(getTransformer('numberTransformer'))
  num?: number

  @codeGen('7758')
  @ApiProperty({
    description: '排序',
    required: false,
  })
  @Transform(getTransformer('numberTransformer'))
  order?: number

  @codeGen('7759')
  @ApiProperty({
    description: '是否系统保留',
    required: true,
  })
  @IsNotEmpty()
  @Transform(getTransformer('booleanTransformer'))
  isSystem: boolean
}

export class UpdateEnumRequestDTO {
  @codeGen('7751')
  @ApiProperty({
    description: '枚举类型id',
    required: false,
  })
  @Transform(getTransformer('numberTransformer'))
  enumTypeId?: number

  @codeGen('7752')
  @ApiProperty({
    description: '中文名字',
    required: false,
  })
  cnName?: string

  @codeGen('7753')
  @ApiProperty({
    description: '英文名字',
    required: false,
  })
  enName?: string

  @codeGen('7754')
  @ApiProperty({
    description: '代码',
    required: false,
  })
  code?: string

  @codeGen('7755')
  @ApiProperty({
    description: '枚举值备注',
    required: false,
  })
  remark?: string

  @codeGen('7756')
  @ApiProperty({
    description: '状态',
    required: false,
  })
  @Transform(getTransformer('booleanTransformer'))
  status?: boolean

  @codeGen('7757')
  @ApiProperty({
    description: '枚举值类型下的具体枚举值的编号',
    required: false,
  })
  @Transform(getTransformer('numberTransformer'))
  num?: number

  @codeGen('7758')
  @ApiProperty({
    description: '排序',
    required: false,
  })
  @Transform(getTransformer('numberTransformer'))
  order?: number

  @codeGen('7759')
  @ApiProperty({
    description: '是否系统保留',
    required: false,
  })
  @Transform(getTransformer('booleanTransformer'))
  isSystem?: boolean
}

export class FindOneEnumRequestDTO {
  @codeGen('7751')
  @ApiProperty({
    description: '枚举类型id',
    required: false,
  })
  @Transform(getTransformer('numberTransformer'))
  enumTypeId?: number

  @codeGen('7752')
  @ApiProperty({
    description: '中文名字',
    required: false,
  })
  cnName?: string

  @codeGen('7753')
  @ApiProperty({
    description: '英文名字',
    required: false,
  })
  enName?: string

  @codeGen('7754')
  @ApiProperty({
    description: '代码',
    required: false,
  })
  code?: string

  @codeGen('7755')
  @ApiProperty({
    description: '枚举值备注',
    required: false,
  })
  remark?: string

  @codeGen('7756')
  @ApiProperty({
    description: '状态',
    required: false,
  })
  @Transform(getTransformer('booleanTransformer'))
  status?: boolean

  @codeGen('7757')
  @ApiProperty({
    description: '枚举值类型下的具体枚举值的编号',
    required: false,
  })
  @Transform(getTransformer('numberTransformer'))
  num?: number

  @codeGen('7758')
  @ApiProperty({
    description: '排序',
    required: false,
  })
  @Transform(getTransformer('numberTransformer'))
  order?: number

  @codeGen('7759')
  @ApiProperty({
    description: '是否系统保留',
    required: false,
  })
  @Transform(getTransformer('booleanTransformer'))
  isSystem?: boolean
}

export class FindAllEnumRequestDTO extends PagingRequestDTO {
  @codeGen('7751')
  @ApiProperty({
    description: '枚举类型id',
    required: false,
  })
  @Transform(getTransformer('numberTransformer'))
  enumTypeId?: number

  @codeGen('7752')
  @ApiProperty({
    description: '中文名字',
    required: false,
  })
  cnName?: string

  @codeGen('7753')
  @ApiProperty({
    description: '英文名字',
    required: false,
  })
  enName?: string

  @codeGen('7754')
  @ApiProperty({
    description: '代码',
    required: false,
  })
  code?: string

  @codeGen('7755')
  @ApiProperty({
    description: '枚举值备注',
    required: false,
  })
  remark?: string

  @codeGen('7756')
  @ApiProperty({
    description: '状态',
    required: false,
  })
  @Transform(getTransformer('booleanTransformer'))
  status?: boolean

  @codeGen('7757')
  @ApiProperty({
    description: '枚举值类型下的具体枚举值的编号',
    required: false,
  })
  @Transform(getTransformer('numberTransformer'))
  num?: number

  @codeGen('7758')
  @ApiProperty({
    description: '排序',
    required: false,
  })
  @Transform(getTransformer('numberTransformer'))
  order?: number

  @codeGen('7759')
  @ApiProperty({
    description: '是否系统保留',
    required: false,
  })
  @Transform(getTransformer('booleanTransformer'))
  isSystem?: boolean

  @Transform(({ value }) => {
    if (value === 'true' || value === '1') return true
    if (value === 'false' || value === '0') return false
    return value
  })
  withDisabled?: boolean
}
