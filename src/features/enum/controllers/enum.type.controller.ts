import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common'
import { ApiOperation, ApiTags } from '@nestjs/swagger'
import {
  FindAllEnumTypeRequestDTO,
  CreateEnumTypeRequestDTO,
  UpdateEnumTypeRequestDTO,
} from './../dto'
import { EnumTypeService } from './../services'
import {
  ApiPaginatedResponse,
  ApiFindOneResponse,
  ApiPatchResponse,
  ApiDeleteResponse,
  codeGen,
} from './../../../core'
import {
  FindOneResponseSchema,
  FindAllResponseSchema,
} from './../dto/enum.type.response.schema'

@Controller('enumType')
@ApiTags('枚举类型表')
export class EnumTypeController {
  constructor(private readonly enumTypeService: EnumTypeService) {}

  @Post('')
  @ApiOperation({
    summary: 'POST enumType',
  })
  @ApiFindOneResponse('enumType', FindOneResponseSchema)
  @codeGen('550-create')
  create(@Body() createEnumType: CreateEnumTypeRequestDTO) {
    return this.enumTypeService.create(createEnumType)
  }

  @Patch(':id')
  @ApiOperation({
    summary: 'PATCH enumType',
  })
  @ApiPatchResponse('enumType')
  @codeGen('550-update')
  update(
    @Param('id') id: string,
    @Body() updateEnumTypeRequestDTO: UpdateEnumTypeRequestDTO,
  ) {
    return this.enumTypeService.updateById(+id, updateEnumTypeRequestDTO)
  }

  @Get('')
  @ApiOperation({
    summary: 'GET enumType(list)',
  })
  @ApiPaginatedResponse('enumType', FindAllResponseSchema)
  @codeGen('550-findAll')
  findAll(@Query() findAllQueryEnumType: FindAllEnumTypeRequestDTO) {
    return this.enumTypeService.findAll(findAllQueryEnumType)
  }

  @Get(':id')
  @ApiOperation({
    summary: 'GET enumType(single)',
  })
  @ApiFindOneResponse('enumType', FindOneResponseSchema)
  @codeGen('550-findOne')
  findOne(@Param('id') id: string) {
    return this.enumTypeService.findOneByIdOrThrow(+id)
  }

  @Delete(':id')
  @ApiOperation({
    summary: 'DELETE enumType',
  })
  @ApiDeleteResponse('enumType')
  @codeGen('550-remove')
  remove(@Param('id') id: string) {
    return this.enumTypeService.removeById(+id)
  }
}
