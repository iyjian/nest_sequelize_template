import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common'
import { ApiOperation, ApiTags } from '@nestjs/swagger'
import {
  FindAllEnumCategoryRequestDTO,
  CreateEnumCategoryRequestDTO,
  UpdateEnumCategoryRequestDTO,
} from './../dto'
import { EnumCategoryService } from './../services'
import {
  ApiPaginatedResponse,
  ApiFindOneResponse,
  ApiPatchResponse,
  ApiDeleteResponse,
  codeGen,
} from './../../../core'
import {
  FindOneResponseSchema,
  FindAllResponseSchema,
} from './../dto/enum.category.response.schema'

@Controller('enumCategory')
@ApiTags('枚举大类表')
export class EnumCategoryController {
  constructor(private readonly enumCategoryService: EnumCategoryService) {}

  @Post('')
  @ApiOperation({
    summary: 'POST enumCategory',
  })
  @ApiFindOneResponse('enumCategory', FindOneResponseSchema)
  @codeGen('551-create')
  create(@Body() createEnumCategory: CreateEnumCategoryRequestDTO) {
    return this.enumCategoryService.create(createEnumCategory)
  }

  @Patch(':id')
  @ApiOperation({
    summary: 'PATCH enumCategory',
  })
  @ApiPatchResponse('enumCategory')
  @codeGen('551-update')
  update(
    @Param('id') id: string,
    @Body() updateEnumCategoryRequestDTO: UpdateEnumCategoryRequestDTO,
  ) {
    return this.enumCategoryService.updateById(
      +id,
      updateEnumCategoryRequestDTO,
    )
  }

  @Get('')
  @ApiOperation({
    summary: 'GET enumCategory(list)',
  })
  @ApiPaginatedResponse('enumCategory', FindAllResponseSchema)
  @codeGen('551-findAll')
  findAll(@Query() findAllQueryEnumCategory: FindAllEnumCategoryRequestDTO) {
    return this.enumCategoryService.findAll(findAllQueryEnumCategory)
  }

  @Get(':id')
  @ApiOperation({
    summary: 'GET enumCategory(single)',
  })
  @ApiFindOneResponse('enumCategory', FindOneResponseSchema)
  @codeGen('551-findOne')
  findOne(@Param('id') id: string) {
    return this.enumCategoryService.findOneByIdOrThrow(+id)
  }

  @Delete(':id')
  @ApiOperation({
    summary: 'DELETE enumCategory',
  })
  @ApiDeleteResponse('enumCategory')
  @codeGen('551-remove')
  remove(@Param('id') id: string) {
    return this.enumCategoryService.removeById(+id)
  }
}
