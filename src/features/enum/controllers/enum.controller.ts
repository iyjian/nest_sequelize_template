import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common'
import { ApiOperation, ApiTags } from '@nestjs/swagger'
import {
  FindAllEnumRequestDTO,
  CreateEnumRequestDTO,
  UpdateEnumRequestDTO,
} from './../dto'
import { EnumService } from './../services'
import {
  ApiPaginatedResponse,
  ApiFindOneResponse,
  ApiPatchResponse,
  ApiDeleteResponse,
  codeGen,
} from './../../../core'
import {
  FindOneResponseSchema,
  FindAllResponseSchema,
} from './../dto/enum.response.schema'

@Controller('enum')
@ApiTags('枚举表')
export class EnumController {
  constructor(private readonly enumService: EnumService) {}

  @Post('')
  @ApiOperation({
    summary: 'POST enum',
  })
  @ApiFindOneResponse('enum', FindOneResponseSchema)
  @codeGen('549-create')
  create(@Body() createEnum: CreateEnumRequestDTO) {
    return this.enumService.create(createEnum)
  }

  @Patch(':id')
  @ApiOperation({
    summary: 'PATCH enum',
  })
  @ApiPatchResponse('enum')
  @codeGen('549-update')
  update(
    @Param('id') id: string,
    @Body() updateEnumRequestDTO: UpdateEnumRequestDTO,
  ) {
    return this.enumService.updateById(+id, updateEnumRequestDTO)
  }

  @Get('')
  @ApiOperation({
    summary: 'GET enum(list)',
  })
  @ApiPaginatedResponse('enum', FindAllResponseSchema)
  @codeGen('549-findAll')
  findAll(@Query() findAllQueryEnum: FindAllEnumRequestDTO) {
    return this.enumService.findAll(findAllQueryEnum)
  }

  @Get(':id')
  @ApiOperation({
    summary: 'GET enum(single)',
  })
  @ApiFindOneResponse('enum', FindOneResponseSchema)
  @codeGen('549-findOne')
  findOne(@Param('id') id: string) {
    return this.enumService.findOneByIdOrThrow(+id)
  }

  @Delete(':id')
  @ApiOperation({
    summary: 'DELETE enum',
  })
  @ApiDeleteResponse('enum')
  @codeGen('549-remove')
  remove(@Param('id') id: string) {
    return this.enumService.removeById(+id)
  }
}
