import {
  Table,
  Column,
  DataType,
  Scopes,
  BelongsTo,
  ForeignKey,
} from 'sequelize-typescript'
import { BaseModel, codeGen } from './../../../core'
import { EnumType } from './'

@Table({
  tableName: 't_enum',
  comment: '枚举表',
})
@codeGen('scopesGen')
@Scopes(() => ({
  findAll: {
    include: [],
  },
  findOne: {
    include: [],
  },
}))
export class Enum extends BaseModel<Enum> {
  @ForeignKey(() => EnumType)
  @Column({
    type: DataType.INTEGER,
    allowNull: true,
    onUpdate: 'NO ACTION',
    onDelete: 'NO ACTION',
    comment: '枚举类型id',
  })
  @codeGen('7751')
  enumTypeId?: number

  @Column({
    allowNull: false,
    type: DataType.STRING(255),
    comment: '中文名字',
  })
  @codeGen('7752')
  cnName: string

  @Column({
    allowNull: true,
    type: DataType.STRING(255),
    comment: '英文名字',
  })
  @codeGen('7753')
  enName?: string

  @Column({
    allowNull: true,
    type: DataType.STRING(255),
    comment: '代码',
  })
  @codeGen('7754')
  code?: string

  @Column({
    allowNull: true,
    type: DataType.STRING(255),
    comment: '枚举值备注',
  })
  @codeGen('7755')
  remark?: string

  @Column({
    allowNull: false,
    type: DataType.BOOLEAN,
    comment: '状态',
  })
  @codeGen('7756')
  status: boolean

  @Column({
    allowNull: true,
    type: DataType.INTEGER,
    comment: '枚举值类型下的具体枚举值的编号',
  })
  @codeGen('7757')
  num?: number

  @Column({
    allowNull: false,
    type: DataType.INTEGER,
    defaultValue: 0,
    comment: '排序',
  })
  @codeGen('7758')
  order: number

  @Column({
    allowNull: false,
    type: DataType.BOOLEAN,
    comment: '是否系统保留',
  })
  @codeGen('7759')
  isSystem: boolean

  @BelongsTo(() => EnumType, 'enumTypeId')
  @codeGen('7772')
  enumType: EnumType

  @Column({
    type: DataType.VIRTUAL,
  })
  get type() {
    return this.enumType?.type
  }

  @Column({
    type: DataType.VIRTUAL,
  })
  get typeDesc() {
    return this.enumType?.cnName
  }

  @Column({
    type: DataType.VIRTUAL,
  })
  get typeEnDesc() {
    return this.enumType?.enName
  }
}
