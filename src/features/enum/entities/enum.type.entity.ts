import {
  Table,
  Column,
  DataType,
  Scopes,
  BelongsTo,
  ForeignKey,
} from 'sequelize-typescript'
import { BaseModel, codeGen } from './../../../core'
import { EnumCategory } from './'

@Table({
  tableName: 't_enum_type',
  comment: '枚举类型表',
})
@codeGen('scopesGen')
@Scopes(() => ({
  findAll: {},
  findOne: {},
}))
export class EnumType extends BaseModel<EnumType> {
  @Column({
    allowNull: false,
    type: DataType.STRING(255),
    comment: '类型代码',
  })
  @codeGen('7760')
  type: string

  @Column({
    allowNull: false,
    type: DataType.STRING(255),
    comment: '类型中文名',
  })
  @codeGen('7761')
  cnName: string

  @Column({
    allowNull: true,
    type: DataType.STRING(255),
    comment: '类型英文名',
  })
  @codeGen('7762')
  enName?: string

  @Column({
    allowNull: false,
    type: DataType.INTEGER,
    comment: '排序值',
  })
  @codeGen('7763')
  order: number

  @ForeignKey(() => EnumCategory)
  @Column({
    type: DataType.INTEGER,
    allowNull: false,
    onUpdate: 'NO ACTION',
    onDelete: 'NO ACTION',
    comment: '枚举分类id',
  })
  @codeGen('7764')
  categoryId: number

  @Column({
    allowNull: true,
    type: DataType.BOOLEAN,
    comment: '是否系统保留',
  })
  @codeGen('7765')
  isSystem?: boolean

  @BelongsTo(() => EnumCategory, 'categoryId')
  @codeGen('7774')
  category: EnumCategory
}
