import { Table, Column, DataType, Scopes } from 'sequelize-typescript'
import { BaseModel, codeGen } from './../../../core'

@Table({
  tableName: 't_enum_category',
  comment: '枚举大类表',
})
@codeGen('scopesGen')
@Scopes(() => ({
  findAll: {},
  findOne: {},
}))
export class EnumCategory extends BaseModel<EnumCategory> {
  @Column({
    allowNull: true,
    type: DataType.STRING(255),
    comment: '分类中文',
  })
  @codeGen('7766')
  cnName?: string

  @Column({
    allowNull: true,
    type: DataType.STRING(255),
    comment: '分类英文',
  })
  @codeGen('7767')
  enName?: string

  @Column({
    allowNull: true,
    type: DataType.STRING(255),
    comment: 'icon的css',
  })
  @codeGen('7768')
  icon?: string

  @Column({
    allowNull: true,
    type: DataType.STRING(255),
    comment: '中文描述',
  })
  @codeGen('7769')
  cnDesc?: string

  @Column({
    allowNull: true,
    type: DataType.STRING(255),
    comment: '英文描述',
  })
  @codeGen('7770')
  enDesc?: string

  @Column({
    allowNull: true,
    type: DataType.INTEGER,
    comment: '排序',
  })
  @codeGen('7771')
  order?: number
}
