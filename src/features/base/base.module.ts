import { Module } from '@nestjs/common'
import { EnumModule } from '../enum'
import { SequelizeModule } from '@nestjs/sequelize'
import { I18nController } from './controllers/i18n.controller'
import { I18nService } from './services/i18n.service'
import { I18N, Log, Route, ClientLog } from './entities'
import { SwaggerController } from './controllers/swagger.controller'
import {
  LogController,
  RouteController,
  MetaController,
  ClientLogController,
} from './controllers'
import { LogService } from './services/log.service'
import { RouteService, MetaService, ClientLogService } from './services'
import { MongooseModule } from '@nestjs/mongoose'
import { LogSchema } from './entities/log.entity'
import { ConfigModule, ConfigService } from '@nestjs/config'

@Module({
  imports: [
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        uri: configService.get('mongo.loggingDBURI'),
      }),
      inject: [ConfigService],
    }),
    SequelizeModule.forFeature([I18N, Route, ClientLog]),
    MongooseModule.forFeature([{ name: Log.name, schema: LogSchema }]),
    EnumModule,
  ],
  controllers: [
    I18nController,
    SwaggerController,
    LogController,
    RouteController,
    MetaController,
    ClientLogController,
  ],
  providers: [
    I18nService,
    RouteService,
    LogService,
    MetaService,
    ClientLogService,
  ],
  exports: [I18nService, RouteService, LogService, ClientLogService],
})
export class BaseModule {}
