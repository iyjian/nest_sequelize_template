import { ApiExcludeController, ApiResponse } from '@nestjs/swagger'
import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common'
import { I18nService } from '../services/i18n.service'
import {
  CreateI18nRequestDTO,
  FindAllI18nDTO,
  UpdateI18nRequestDTO,
} from '../dto'

@Controller('i18N')
@ApiExcludeController()
export class I18nController {
  constructor(private readonly i18NService: I18nService) {}

  @Post('')
  create(@Body() createI18n: CreateI18nRequestDTO) {
    return this.i18NService.createI18n(createI18n)
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateI18nDTO: UpdateI18nRequestDTO) {
    return this.i18NService.updateI18n(+id, updateI18nDTO)
  }

  @Get('')
  findAll(@Query() findAllQueryI18n: FindAllI18nDTO) {
    return this.i18NService.findAllI18n(findAllQueryI18n)
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.i18NService.findOneI18n(+id)
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.i18NService.removeI18n(+id)
  }
}
