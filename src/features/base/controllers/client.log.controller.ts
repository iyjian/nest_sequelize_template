import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common'
import { ApiOperation, ApiTags } from '@nestjs/swagger'
import {
  FindAllClientLogRequestDTO,
  CreateClientLogRequestDTO,
  UpdateClientLogRequestDTO,
} from './../dto'
import { ClientLogService } from './../services'
import {
  ApiPaginatedResponse,
  ApiFindOneResponse,
  ApiPatchResponse,
  ApiDeleteResponse,
  codeGen,
} from './../../../core'
import {
  FindOneResponseSchema,
  FindAllResponseSchema,
} from './../dto/client.log.response.schema'

@Controller('clientLog')
@ApiTags('设备日志')
export class ClientLogController {
  constructor(private readonly clientLogService: ClientLogService) {}

  @Post('')
  @ApiOperation({
    summary: 'POST clientLog',
  })
  // @ApiFindOneResponse('clientLog', FindOneResponseSchema)
  @ApiPatchResponse('clientLog')
  @codeGen('660-create')
  async create(@Body() createClientLog: CreateClientLogRequestDTO) {
    await this.clientLogService.create(createClientLog)
    return true
  }

  @Patch(':id')
  @ApiOperation({
    summary: 'PATCH clientLog',
  })
  @ApiPatchResponse('clientLog')
  @codeGen('660-update')
  update(
    @Param('id') id: string,
    @Body() updateClientLogRequestDTO: UpdateClientLogRequestDTO,
  ) {
    return this.clientLogService.updateById(+id, updateClientLogRequestDTO)
  }

  @Get('')
  @ApiOperation({
    summary: 'GET clientLog(list)',
  })
  @ApiPaginatedResponse('clientLog', FindAllResponseSchema)
  @codeGen('660-findAll')
  findAll(@Query() findAllQueryClientLog: FindAllClientLogRequestDTO) {
    return this.clientLogService.findAll(findAllQueryClientLog)
  }

  @Get(':id')
  @ApiOperation({
    summary: 'GET clientLog(single)',
  })
  @ApiFindOneResponse('clientLog', FindOneResponseSchema)
  @codeGen('660-findOne')
  findOne(@Param('id') id: string) {
    return this.clientLogService.findOneByIdOrThrow(+id)
  }

  @Delete(':id')
  @ApiOperation({
    summary: 'DELETE clientLog',
  })
  @ApiDeleteResponse('clientLog')
  @codeGen('660-remove')
  remove(@Param('id') id: string) {
    return this.clientLogService.removeById(+id)
  }
}
