import { ApiExcludeController } from '@nestjs/swagger'
import { Controller, Get, Inject, Logger, Res } from '@nestjs/common'
import { CACHE_MANAGER } from '@nestjs/cache-manager'
import { Cache } from 'cache-manager'
import { Response } from 'express'
import { ConfigService } from '@nestjs/config'
import { AppConfig } from '../../../config'

@Controller('swagger')
@ApiExcludeController()
export class SwaggerController {
  private readonly logger = new Logger(SwaggerController.name)

  constructor(
    @Inject(CACHE_MANAGER) public cacheManager: Cache,
    private readonly configService: ConfigService,
  ) {}

  @Get('')
  async findAll(@Res() res: Response) {
    const appConfig = this.configService.get<AppConfig>(`app`)

    const result = await this.cacheManager.get<object>(
      `swagger-sepc-${appConfig.code}`,
    )

    if (!result) {
      this.logger.error(`swagger spec not cached`)
      res.send('')
      return
    }

    res.set('Content-Type', 'application/json')
    res.set('Content-Disposition', `attachment;filename=swagger-spec.json`)
    res.send(Buffer.from(JSON.stringify(result)))
  }

  @Get('json')
  async getJson() {
    const appConfig = this.configService.get<AppConfig>(`app`)
    const result = await this.cacheManager.get<object>(
      `swagger-sepc-${appConfig.code}`,
    )
    return result
  }
}
