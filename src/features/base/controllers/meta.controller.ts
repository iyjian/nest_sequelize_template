import { Controller, Get, Query } from '@nestjs/common'
import { ApiOperation, ApiTags } from '@nestjs/swagger'
import { MetaService } from './../services'
import { GetColumnDiffRequestDTO } from '../dto'

@Controller('meta')
@ApiTags('元数据')
export class MetaController {
  constructor(private readonly metaService: MetaService) {}

  @Get('column/diff')
  @ApiOperation({
    summary: '获取当前数据库字段与模型定义的diff',
  })
  findAll(@Query() getColumnDiffRequest: GetColumnDiffRequestDTO) {
    return this.metaService.getColumnsMeta(getColumnDiffRequest)
  }
}
