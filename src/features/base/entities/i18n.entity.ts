import { BaseModel } from '../../../core/entities'
import { Table, Column, DataType } from 'sequelize-typescript'
/**
  insert into t_i18n (isTableColumn, tableName, columnName, columnComment, columnDataType, cnName, createdAt, updatedAt, syncKey)
  select  1,
          table_name `表名`,
          column_name `字段名`, 
          column_comment `字段描述`,
          substr(column_type,1,255) `字段类型`, 
          column_comment,
          now(),
          now(),
          substr(uuid(), 1, 32)
  from information_schema.columns 
  where table_schema = 'fleet_wc' and table_name <> 'deleteme' and column_name not in ('syncKey', 'createdAt', 'updatedAt', 'isActive')
  order by length(column_type) desc;
 */
@Table({
  tableName: 't_i18n',
  timestamps: true,
  indexes: [
    {
      fields: ['isTableColumn', 'tableName', 'columnName', 'isActive'],
      unique: true,
    },
  ],
})
export class I18N extends BaseModel<I18N> {
  @Column({
    type: DataType.TINYINT,
    // allowNull: false,
    comment: '是否为数据表字段',
  })
  isTableColumn: boolean

  @Column({
    type: DataType.STRING(255),
    // allowNull: false,
    defaultValue: '',
    comment: '表名',
  })
  tableName: string

  @Column({
    type: DataType.STRING(255) + ' CHARSET utf8 COLLATE utf8_bin',
    // allowNull: false,
    comment: '字段名',
  })
  columnName: string

  @Column({
    type: DataType.STRING(255),
    comment: '字段描述',
  })
  columnComment: string

  @Column({
    type: DataType.STRING(255),
    comment: '字段数据类型',
  })
  columnDataType: string

  @Column({
    type: DataType.STRING(255),
    comment: '中文字段名',
  })
  cnName!: string

  @Column({
    type: DataType.STRING(255),
    comment: '英文字段名',
  })
  enName!: string

  @Column({
    type: DataType.TEXT,
    comment: '其他备注',
  })
  remark: string
}
