import { Table, Column, DataType, Scopes } from 'sequelize-typescript'
import { codeGen, BaseModel } from './../../../core'

@Table({
  tableName: 't_client_log',
  comment: '设备日志',
})
@codeGen('scopesGen')
@Scopes(() => ({
  findAll: {
    include: [],
  },
  findOne: {
    include: [],
  },
}))
export class ClientLog extends BaseModel<ClientLog> {
  @Column({
    allowNull: false,
    type: DataType.STRING(40),
    comment: '客户端id',
  })
  @codeGen('9066')
  identification: string

  @Column({
    allowNull: false,
    type: DataType.TEXT,
    comment: '报错堆栈',
  })
  @codeGen('9067')
  exception: string

  @Column({
    allowNull: true,
    type: DataType.STRING(40),
    comment: 'ip地址',
  })
  @codeGen('9068')
  ip?: string

  @Column({
    allowNull: false,
    type: DataType.BIGINT,
    comment: '客户端时间',
  })
  @codeGen('9069')
  clientTime: number

  @Column({
    allowNull: true,
    type: DataType.TEXT,
    comment: 'exception的message',
  })
  @codeGen('9070')
  message?: string
}
