import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { HydratedDocument } from 'mongoose'

export type LogDocument = HydratedDocument<Log>

@Schema({
  timestamps: true,
})
export class Log {
  @Prop({
    index: true,
  })
  id: string

  @Prop()
  userId: string

  @Prop()
  routeId: number

  @Prop({
    index: true,
  })
  method: string

  @Prop({
    index: true,
  })
  path: string

  @Prop({
    index: 'text',
  })
  url: string

  @Prop({
    index: 'text',
  })
  payload: string

  @Prop({
    index: 'text',
  })
  ip: string

  // @Prop()
  // user: any

  @Prop({
    index: true,
  })
  requestDate: Date

  @Prop()
  statusCode: number

  @Prop()
  duration: number
}

export const LogSchema = SchemaFactory.createForClass(Log)
