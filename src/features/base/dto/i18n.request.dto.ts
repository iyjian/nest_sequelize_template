import { PagingRequestDTO } from '../../../core/interfaces/request.dto'
import { Transform, Type } from 'class-transformer'
import { ApiProperty } from '@nestjs/swagger'
import { codeGen } from './../../../core'

export class CreateI18nRequestDTO {
  @Transform(({ value }) => {
    if (value === 'true' || value === '1') return true
    if (value === 'false' || value === '0') return false
    return value
  })
  @ApiProperty({
    description: '是否为表名',
    required: false,
  })
  @codeGen('legacy')
  isTableColumn: boolean

  @ApiProperty({
    description: '表名',
    required: false,
  })
  @codeGen('legacy')
  tableName: string
  @ApiProperty({
    description: '字段名',
    required: false,
  })
  @codeGen('legacy')
  columnName: string
  @ApiProperty({
    description: '字段含义',
    required: false,
  })
  @codeGen('legacy')
  columnComment: string
  @ApiProperty({
    description: '字段类型',
    required: false,
  })
  @codeGen('legacy')
  columnDataType: string
  @ApiProperty({
    description: '中文名',
    required: false,
  })
  @codeGen('legacy')
  cnName: string
  @ApiProperty({
    description: '英文名',
    required: false,
  })
  @codeGen('legacy')
  enName: string
  @ApiProperty({
    description: '备注',
    required: false,
  })
  @codeGen('legacy')
  remark: string
}

export class UpdateI18nRequestDTO {
  @Transform(({ value }) => {
    if (value === 'true' || value === '1') return true
    if (value === 'false' || value === '0') return false
    return value
  })
  @ApiProperty({
    description: '是否为表名',
    required: false,
  })
  @codeGen('legacy')
  isTableColumn: boolean

  @ApiProperty({
    description: '表名',
    required: false,
  })
  @codeGen('legacy')
  tableName: string
  @ApiProperty({
    description: '字段名',
    required: false,
  })
  @codeGen('legacy')
  columnName: string
  @ApiProperty({
    description: '字段含义',
    required: false,
  })
  @codeGen('legacy')
  columnComment: string
  @ApiProperty({
    description: '字段类型',
    required: false,
  })
  @codeGen('legacy')
  columnDataType: string
  @ApiProperty({
    description: '中文名',
    required: false,
  })
  @codeGen('legacy')
  cnName: string
  @ApiProperty({
    description: '英文名',
    required: false,
  })
  @codeGen('legacy')
  enName: string
  @ApiProperty({
    description: '备注',
    required: false,
  })
  @codeGen('legacy')
  remark: string
}

export class FindAllI18nDTO extends PagingRequestDTO {
  @Transform(({ value }) => {
    if (value === 'true' || value === '1') return true
    if (value === 'false' || value === '0') return false
    return value
  })
  @ApiProperty({
    description: '是否为表名',
    required: false,
  })
  @codeGen('legacy')
  isTableColumn: boolean

  @ApiProperty({
    description: '表名',
    required: false,
  })
  @codeGen('legacy')
  tableName: string
  @ApiProperty({
    description: '字段名',
    required: false,
  })
  @codeGen('legacy')
  columnName: string
  @ApiProperty({
    description: '字段含义',
    required: false,
  })
  @codeGen('legacy')
  columnComment: string
  @ApiProperty({
    description: '字段类型',
    required: false,
  })
  @codeGen('legacy')
  columnDataType: string
  @ApiProperty({
    description: '中文名',
    required: false,
  })
  @codeGen('legacy')
  cnName: string
  @ApiProperty({
    description: '英文名',
    required: false,
  })
  @codeGen('legacy')
  enName: string
  @ApiProperty({
    description: '备注',
    required: false,
  })
  @codeGen('legacy')
  remark: string
}
