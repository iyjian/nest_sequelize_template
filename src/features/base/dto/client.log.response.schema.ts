export const FindOneResponseSchema = {
  identification: { type: 'string', example: '', description: '客户端id' },
  exception: { type: 'string', example: '', description: '报错堆栈' },
  ip: { type: 'string', example: '', description: 'ip地址' },
  clientTime: { type: 'number', example: '', description: '客户端时间' },
  message: { type: 'string', example: '', description: 'exception的message' },
}

export const FindAllResponseSchema = {
  identification: { type: 'string', example: '', description: '客户端id' },
  exception: { type: 'string', example: '', description: '报错堆栈' },
  ip: { type: 'string', example: '', description: 'ip地址' },
  clientTime: { type: 'number', example: '', description: '客户端时间' },
  message: { type: 'string', example: '', description: 'exception的message' },
}
