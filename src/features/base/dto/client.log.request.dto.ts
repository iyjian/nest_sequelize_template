import { PagingRequestDTO, getTransformer, codeGen } from './../../../core'
import { Transform, Type } from 'class-transformer'
import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty } from 'class-validator'

export class CreateClientLogRequestDTO {
  @codeGen('9066')
  @ApiProperty({
    description: '客户端id',
    required: true,
  })
  @IsNotEmpty()
  identification: string

  @codeGen('9067')
  @ApiProperty({
    description: '报错堆栈',
    required: true,
  })
  @IsNotEmpty()
  exception: string

  ip?: string

  @codeGen('9069')
  @ApiProperty({
    description: '客户端时间',
    required: true,
  })
  @IsNotEmpty()
  @Transform(getTransformer('numberTransformer'))
  clientTime: number

  @codeGen('9070')
  @ApiProperty({
    description: 'exception的message',
    required: false,
  })
  message?: string
}

export class UpdateClientLogRequestDTO {
  @codeGen('9066')
  @ApiProperty({
    description: '客户端id',
    required: false,
  })
  identification?: string

  @codeGen('9067')
  @ApiProperty({
    description: '报错堆栈',
    required: false,
  })
  exception?: string

  ip?: string

  @codeGen('9069')
  @ApiProperty({
    description: '客户端时间',
    required: false,
  })
  @Transform(getTransformer('numberTransformer'))
  clientTime?: number

  @codeGen('9070')
  @ApiProperty({
    description: 'exception的message',
    required: false,
  })
  message?: string
}

export class FindOneClientLogRequestDTO {
  @codeGen('9066')
  @ApiProperty({
    description: '客户端id',
    required: false,
  })
  identification?: string

  @codeGen('9067')
  @ApiProperty({
    description: '报错堆栈',
    required: false,
  })
  exception?: string

  @codeGen('9068')
  @ApiProperty({
    description: 'ip地址',
    required: false,
  })
  ip?: string

  @codeGen('9069')
  @ApiProperty({
    description: '客户端时间',
    required: false,
  })
  @Transform(getTransformer('numberTransformer'))
  clientTime?: number

  @codeGen('9070')
  @ApiProperty({
    description: 'exception的message',
    required: false,
  })
  message?: string
}

export class FindAllClientLogRequestDTO extends PagingRequestDTO {
  @codeGen('9066')
  @ApiProperty({
    description: '客户端id',
    required: false,
  })
  identification?: string

  @codeGen('9067')
  @ApiProperty({
    description: '报错堆栈',
    required: false,
  })
  exception?: string

  @codeGen('9068')
  @ApiProperty({
    description: 'ip地址',
    required: false,
  })
  ip?: string

  @codeGen('9069')
  @ApiProperty({
    description: '客户端时间',
    required: false,
  })
  @Transform(getTransformer('numberTransformer'))
  clientTime?: number

  @codeGen('9070')
  @ApiProperty({
    description: 'exception的message',
    required: false,
  })
  message?: string
}
