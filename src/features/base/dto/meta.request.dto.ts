import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty } from 'class-validator'

export class GetColumnDiffRequestDTO {
  @ApiProperty({
    description: 'devToolApi',
    required: true,
  })
  @IsNotEmpty()
  devToolApi: string

  @ApiProperty({
    description: 'devToolToken',
    required: true,
  })
  @IsNotEmpty()
  devToolToken: string

  @ApiProperty({
    description: 'projectId',
    required: true,
  })
  @IsNotEmpty()
  projectId: number
}
