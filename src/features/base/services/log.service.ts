import { Injectable } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { Model } from 'mongoose'
import { BaseService } from './../../../core'
import { Log } from './../entities'
import {
  FindAllLogRequestDTO,
  FindOneLogRequestDTO,
  CreateLogRequestDTO,
  UpdateLogRequestDTO,
} from './../dto'

@Injectable()
export class LogService extends BaseService {
  private readonly include: any[]

  constructor(@InjectModel(Log.name) private readonly logModel: Model<Log>) {
    super()
    this.include = []
  }

  create(createLogRequest: CreateLogRequestDTO) {
    return this.logModel.create(createLogRequest)
  }

  async findAll(findAllLogRequest: FindAllLogRequestDTO) {
    const {
      page = 1,
      pageSize = 20,
      skipPaging,
      search = '',
      sort = [['requestDate', 'desc']],
      ...payload
    } = findAllLogRequest

    const searchCondition = this.normallizeMongoCondition(payload)

    if (search) {
      searchCondition['$text'] = { $search: search }
    }

    const mongoSort = sort.map((sortCondition) => ({
      [sortCondition[0]]: sortCondition[1] === 'desc' ? -1 : 1,
    }))

    const rows = await this.logModel
      .find(searchCondition, null, {
        skip: (page - 1) * pageSize,
        limit: pageSize,
        sort: mongoSort,
      })
      .exec()

    const count = await this.logModel.count(searchCondition)

    return {
      rows,
      count,
    }
  }

  findOneById(id: string) {
    return this.logModel.findOne({ id }).exec()
  }

  async updateByRequestId(id: string, updateLogRequest: UpdateLogRequestDTO) {
    return this.logModel.updateOne({ id }, updateLogRequest)
  }

  /*

  // async findOne(
  //   findOneLogRequest: FindOneLogRequestDTO,
  // ) {
  //   const log = await this.logModel.findOne({
  //     where: { ...findOneLogRequest },
  //     include: this.include,
  //     transaction,
  //   })
  //   return log
  // }

  async updateById(
    id: number,
    updateLogRequest: UpdateLogRequestDTO,
  ) {
    await this.logModel.update(updateLogRequest, {
      where: {
        id,
      },
      transaction,
    })
    return true
  }

  */
}
