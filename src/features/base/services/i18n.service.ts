import { Injectable } from '@nestjs/common'
import { InjectModel } from '@nestjs/sequelize'
import { BaseService } from '../../../core'
import { Op, Transaction } from 'sequelize'
import {
  UpdateI18nRequestDTO,
  CreateI18nRequestDTO,
  FindAllI18nDTO,
} from '../dto'
import { I18N } from '../entities/i18n.entity'
import { Sequelize } from 'sequelize-typescript'

@Injectable()
export class I18nService extends BaseService {
  private readonly include: any[]

  constructor(
    @InjectModel(I18N)
    private readonly i18NModel: typeof I18N,
    private readonly mysql: Sequelize,
  ) {
    super()

    this.include = []
  }

  async createI18n(i18NObj: CreateI18nRequestDTO) {
    const i18N = await this.i18NModel.create(i18NObj)
    return this.findOneI18n(i18N.id)
  }

  async findAllI18n(findAllQueryI18n: FindAllI18nDTO) {
    const {
      page,
      pageSize,
      skipPaging,
      search,
      sort = [['id', 'desc']],
      ...payload
    } = findAllQueryI18n

    const i18NS = await this.i18NModel.findAndCountAll({
      where: {},
      offset: skipPaging ? undefined : (page - 1) * pageSize,
      limit: skipPaging ? undefined : pageSize,
      include: this.include,
      order: sort,
    })

    return i18NS
  }

  async findByIdsI18n(ids: number[]) {
    const i18NS = await this.i18NModel.findAll({
      where: {
        id: {
          [Op.in]: ids,
        },
      },
    })

    return i18NS
  }

  async findOneI18n(id: number) {
    const i18N = await this.i18NModel.findByPk(id, {
      include: this.include,
    })
    return i18N
  }

  async updateI18n(id: number, updateI18nDTO: UpdateI18nRequestDTO) {
    await this.i18NModel.update(updateI18nDTO, {
      where: {
        id,
      },
    })
    return true
  }

  async removeI18n(id: number, transaction?: Transaction) {
    const isOuterTransaction = !!transaction
    try {
      if (!transaction) {
        transaction = await this.mysql.transaction()
      }

      await this.i18NModel.update(
        { isActive: null },
        {
          where: {
            id,
          },
          transaction,
        },
      )

      if (!isOuterTransaction) {
        await transaction.commit()
      }
      return true
    } catch (e) {
      if (transaction && !isOuterTransaction) {
        await transaction.rollback()
        if (e.toString() === 'Error: SequelizeForeignKeyConstraintError') {
          throw new Error('有依赖数据，无法删除')
        }
      }
      throw e
    }
  }
}
