import { Injectable, HttpException, HttpStatus, Logger } from '@nestjs/common'
import { InjectModel } from '@nestjs/sequelize'
import { Op, Transaction } from 'sequelize'
import { Sequelize } from 'sequelize-typescript'
import { BaseService } from './../../../core'
import { ClientLog } from './../entities'
import {
  FindAllClientLogRequestDTO,
  FindOneClientLogRequestDTO,
  CreateClientLogRequestDTO,
  UpdateClientLogRequestDTO,
} from './../dto'

@Injectable()
export class ClientLogService extends BaseService {
  private readonly include: any[]

  constructor(
    @InjectModel(ClientLog)
    private readonly clientLogModel: typeof ClientLog,
    private readonly mysql: Sequelize,
  ) {
    super()
    this.include = []
  }

  async create(
    createClientLogRequest: CreateClientLogRequestDTO,
    transaction?: Transaction,
  ) {
    const isOuterTransaction = !!transaction
    try {
      if (!isOuterTransaction) {
        transaction = await this.mysql.transaction()
      }

      const clientLog = await this.clientLogModel.create(
        createClientLogRequest,
        { transaction },
      )

      if (!isOuterTransaction) {
        await transaction.commit()
        return this.findOneById(clientLog.id)
      } else {
        return this.findOneById(clientLog.id, transaction)
      }
    } catch (e) {
      if (!isOuterTransaction && transaction) {
        await transaction.rollback()
      }
      throw e
    }
  }

  async findAll(
    findAllClientLogRequest: FindAllClientLogRequestDTO,
    transaction?: Transaction,
  ) {
    const {
      page = 1,
      pageSize = 20,
      skipPaging,
      search,
      sort = [['id', 'desc']],
      ...payload
    } = findAllClientLogRequest

    const condition = this.normalizeCondition(payload)

    if (search) {
      condition[Op.or] = {}
    }

    const clientLogs = await this.clientLogModel
      .scope(['defaultScope', 'findAll'])
      .findAndCountAll({
        where: { ...condition },
        offset: skipPaging ? undefined : (page - 1) * pageSize,
        limit: skipPaging ? undefined : pageSize,
        order: sort,
        transaction,
      })

    return clientLogs
  }

  async findOneById(id: number, transaction?: Transaction) {
    const clientLog = await this.clientLogModel
      .scope(['defaultScope', 'findOne'])
      .findByPk(id, {
        transaction,
      })

    return clientLog
  }

  async findByIds(ids: number[], transaction?: Transaction) {
    const clientLogs = await this.clientLogModel.findAll({
      where: {
        id: {
          [Op.in]: ids,
        },
      },
      transaction,
    })

    return clientLogs
  }

  async findOneByIdOrThrow(id: number, transaction?: Transaction) {
    const clientLog = await this.clientLogModel
      .scope(['defaultScope', 'findOne'])
      .findByPk(id, {
        transaction,
      })

    if (!clientLog) {
      throw new HttpException('未找到该记录', HttpStatus.NOT_FOUND)
    }

    return clientLog
  }

  async findOneOrThrow(
    findOneClientLogRequest: FindOneClientLogRequestDTO,
    transaction?: Transaction,
  ) {
    const clientLog = await this.clientLogModel
      .scope(['defaultScope', 'findOne'])
      .findOne({
        where: { ...findOneClientLogRequest },
        transaction,
      })

    if (!clientLog) {
      throw new HttpException('未找到该记录', HttpStatus.NOT_FOUND)
    }

    return clientLog
  }

  async findOne(
    findOneClientLogRequest: FindOneClientLogRequestDTO,
    transaction?: Transaction,
  ) {
    const clientLog = await this.clientLogModel
      .scope(['defaultScope', 'findOne'])
      .findOne({
        where: { ...findOneClientLogRequest },
        transaction,
      })

    return clientLog
  }

  async updateById(
    id: number,
    updateClientLogRequest: UpdateClientLogRequestDTO,
    transaction?: Transaction,
  ) {
    await this.clientLogModel.update(updateClientLogRequest, {
      where: {
        id,
      },
      transaction,
    })
    return true
  }

  async removeById(id: number, transaction?: Transaction) {
    const isOuterTransaction = !!transaction
    try {
      if (!transaction) {
        transaction = await this.mysql.transaction()
      }

      await this.clientLogModel.update(
        { isActive: null },
        {
          where: {
            id,
          },
          transaction,
        },
      )

      if (!isOuterTransaction) {
        await transaction.commit()
      }
      return true
    } catch (e) {
      if (transaction && !isOuterTransaction) {
        await transaction.rollback()
        if (e.toString() === 'Error: SequelizeForeignKeyConstraintError') {
          throw new Error('有依赖数据，无法删除')
        }
      }
      throw e
    }
  }
}
