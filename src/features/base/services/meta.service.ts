import { Injectable, Logger } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import _ from 'lodash'
import { QueryTypes } from 'sequelize'
import { Sequelize } from 'sequelize-typescript'
import axios from 'axios'
import qs from 'qs'
import { GetColumnDiffRequestDTO } from '../dto'

type COLUMN_DEFINITION = {
  tableName: string
  columnName: string
  allowNull: number
  dataType: string
  defaultValue: string
  columnComment: string
  refTableName: string
  refColumnName: string
  constraintName: string
  hasConstraintData: Boolean
  hasTouched: number
}

@Injectable()
export class MetaService {
  private readonly logger = new Logger(MetaService.name)
  private readonly mysqlDefinitionSQL = `
  with relationColumn as (
    SELECT  table_name              as tableName,
            column_name             as columnName,
            max(referenced_table_name)   as refTableName, 
            max(referenced_column_name)  as refColumnName,
            max(constraint_name)         as constraintName
    FROM information_schema.KEY_COLUMN_USAGE 
    WHERE referenced_table_name is not null and table_schema = ?
    group by table_name,
             column_name
  )
  SELECT  table_name        as tableName,
          column_name       as columnName, 
          case is_nullable 
          when 'YES' 
          then 1 
          else 0 
          end               as allowNull, 
          column_type       as dataType, 
          column_default    as defaultValue,
          column_comment    as columnComment,
          t.refTableName    as refTableName,
          t.refColumnName   as refColumnName,
          t.constraintName  as constraintName,
          0                 as hasTouched
  FROM information_schema.columns s
  LEFT JOIN relationColumn        t on s.table_name = t.tableName and 
                                       s.column_name = t.columnName
  where s.table_schema = ?;`
  constructor(
    private readonly mysql: Sequelize,
    private readonly configService: ConfigService,
  ) {}

  private async getTableRows(tableName: string) {
    const result = await this.mysql.query(
      `select count(1) as count from ${tableName}`,
      { type: QueryTypes.SELECT },
    )
    return result[0]['count']
  }

  private async getAllDefinedRelationColumns(
    devToolApi: string,
    devToolToken: string,
    projectId: number,
  ) {
    const response = await axios.get(`${devToolApi}/metaColumn`, {
      params: {
        token: devToolToken,
        refTableId: {
          isnull: false,
        },
        'dataType.dataType': {
          ne: 'vrelation',
        },
        'table.projectId': projectId,
        skipPaging: true,
      },
      paramsSerializer: {
        serialize: (params) => {
          return qs.stringify(params, { arrayFormat: 'brackets' })
        },
      },
    })

    if (response.data.err) {
      throw new Error(response.data.errMsg)
    }

    return response.data.data.rows
  }

  private async getMigrateSql(
    projectId: number,
    columnDefinitions: any,
    devToolApi: string,
    devToolToken: string,
  ) {
    const response = await axios.post(
      `${devToolApi}/dbsync/column/diff/production?token=${devToolToken}`,
      {
        projectId,
        columnDefinitions,
      },
    )

    if (response.data.err) {
      throw new Error(response.data.errMsg)
    }

    return response.data.data
  }

  public async getColumnsMeta(getColumnDiffRequest: GetColumnDiffRequestDTO) {
    const relationColumns = await this.getAllDefinedRelationColumns(
      getColumnDiffRequest.devToolApi,
      getColumnDiffRequest.devToolToken,
      getColumnDiffRequest.projectId,
    )

    const keyedRelationColumns = _.keyBy(
      relationColumns,
      (relationColumn) => `${relationColumn.table.name}-${relationColumn.name}`,
    )

    const databaseDefinitions = await this.mysql.query<
      Partial<COLUMN_DEFINITION>
    >(this.mysqlDefinitionSQL, {
      replacements: [
        this.configService.get('mysql.db'),
        this.configService.get('mysql.db'),
      ],
      type: QueryTypes.SELECT,
    })

    for (const column of databaseDefinitions) {
      if (column.columnName === 'parentCategoryId') {
        console.log(JSON.stringify(column, null, 2))
      }
      if (
        `${column.tableName}-${column.columnName}` in keyedRelationColumns &&
        !column.refTableName
      ) {
        // 是模型里的关系字段，但是refTableName不为空
        const hasConstraintData =
          (await this.getTableRows(
            keyedRelationColumns[`${column.tableName}-${column.columnName}`]
              .refTable.name,
          )) > 0
        column.hasConstraintData = hasConstraintData
        if (column.columnName === 'parentCategoryId') {
          console.log(JSON.stringify(column, null, 2))
        }
        this.logger.debug(
          `update Column - ${column.tableName}-${column.columnName} hasConstraintData: ${hasConstraintData}`,
        )
      }
    }

    const finalResult = _.groupBy(databaseDefinitions, 'tableName')

    for (const relationColumn of relationColumns) {
      const tableName = relationColumn.table.name
      const columnName = relationColumn.name
      const refTableName = relationColumn.refTable.name
      if (!finalResult[tableName]) {
        continue
      }
      if (
        _.findIndex(
          finalResult[tableName],
          (column) => column.columnName == columnName,
        ) === -1
      ) {
        finalResult[tableName].push({
          tableName,
          columnName,
          refTableName,
          hasConstraintData: (await this.getTableRows(refTableName)) > 0,
        })
        this.logger.debug(
          `getColumnsMeta - add column for query constraintData - tableName: ${tableName} columnName: ${columnName}`,
        )
      }
    }

    const result = await this.getMigrateSql(
      getColumnDiffRequest.projectId,
      finalResult,
      getColumnDiffRequest.devToolApi,
      getColumnDiffRequest.devToolToken,
    )

    for (const key in result) {
      if (!result['key']?.length) {
        delete result[key]
      }
    }

    return result
  }
}
