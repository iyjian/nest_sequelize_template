import { BaseModel } from '../../../core/entities'
import {
  Table,
  Column,
  DataType,
  Scopes,
  AfterFind,
} from 'sequelize-typescript'
import { codeGen } from '../../../core'
import * as Minio from 'minio'
import 'dotenv/config'
import ConfigService from '../../../config/configuration'
import signing = require('minio/dist/main/signing')

const configService = ConfigService()

const minioClientConfig = {
  accessKey: configService.minio.accessKey,
  secretKey: configService.minio.secretKey,
  endPoint: configService.minio.server,
  port: configService.minio.port,
  useSSL: false,
}

const bucketMapping = {
  [configService.minio.bucket]: {
    isPublic: false,
    thumbnailBucket: configService.minio.thumbnailBucket,
  },
  [configService.minio.publicBucket]: {
    isPublic: true,
    thumbnailBucket: configService.minio.publicThumbnailBucket,
  },
}

const client = new Minio.Client(minioClientConfig)

@Table({
  tableName: 't_attachment',
  timestamps: true,
  indexes: [
    { fields: ['isFolder'] },
    { fields: ['createdAt'] },
    { fields: ['path'] },
  ],
})
@codeGen('scopesGen')
@Scopes(() => ({
  findAll: {
    include: [],
  },
  findOne: {
    include: [],
  },
}))
export class Attachment extends BaseModel<Attachment> {
  @Column({
    type: DataType.STRING(40),
    // allowNull: false,
    comment: '桶',
  })
  bucket: string

  @Column({
    type: DataType.VIRTUAL,
    comment: '缩略图桶',
  })
  get thumbnailBucket() {
    return `${this.getDataValue('bucket')}-thumbnail`
  }

  @Column({
    type: DataType.STRING(100),
    // allowNull: false,
    comment: '文件对象id',
  })
  objectId: string

  @Column({
    type: DataType.STRING(255),
    // allowNull: false,
    comment: '文件名',
  })
  objectName: string

  @Column({
    type: DataType.BOOLEAN,
    // allowNull: false,
    comment: '是否是文件夹',
  })
  isFolder: boolean

  @Column({
    // type: DataType.ENUM('image/gif', 'image/gif', 'text/html', 'image/jpeg', 'video/mpeg', 'text/csv', 'image/png),
    type: DataType.STRING(255),
    // allowNull: false,
    comment: '文件类型',
  })
  mimeType: string

  @Column({
    type: DataType.INTEGER,
    // allowNull: false,
    comment: '文件大小',
  })
  size: number

  @Column({
    type: DataType.STRING,
    allowNull: true,
    comment: '备注',
  })
  remark: string

  @Column({
    type: DataType.STRING,
    // allowNull: false,
    comment: '来源系统',
  })
  sourceSystem: string

  @Column({
    type: DataType.STRING,
    // allowNull: false,
    comment: '文件路径',
  })
  path: string

  @Column({
    type: DataType.VIRTUAL,
  })
  get url(): string {
    const bucket = this.getDataValue('bucket')
    const objectId = this.getDataValue('objectId')
    const path = this.getDataValue('path')
    if (bucket && path && objectId) {
      if (bucketMapping[bucket].isPublic) {
        return `/${bucket}${path}${objectId}`
      } else {
        return this.preSign(bucket, objectId)
      }
    }
  }

  @Column({
    type: DataType.VIRTUAL,
  })
  get thumbnailUrl(): string {
    const bucket = this.getDataValue('bucket')
    const objectId = this.getDataValue('objectId')
    const path = this.getDataValue('path')
    if (/^image/.test(this.getDataValue('mimeType')) && bucket && objectId) {
      const thumbnailBucket = bucketMapping[bucket].thumbnailBucket
      if (bucketMapping[bucket].isPublic) {
        return `/${thumbnailBucket}${path}${objectId}`
      } else {
        return this.preSign(thumbnailBucket, objectId)
      }
    }
  }

  // public async genUrl() {
  //   const bucket = this.getDataValue('bucket')
  //   if (bucketMapping[bucket].isPublic) {
  //     this.setDataValue(
  //       'url',
  //       bucket + this.getDataValue('path') + this.getDataValue('objectId'),
  //     )
  //   } else {
  //     const url = await client.presignedUrl(
  //       'GET',
  //       this.getDataValue('bucket'),
  //       this.getDataValue('path').substring(1) + this.getDataValue('objectId'),
  //       60,
  //     )
  //     this.setDataValue(
  //       'url',
  //       url.replace(
  //         new RegExp(
  //           `http://${configService.minio.server}:${configService.minio.port}`,
  //         ),
  //         '',
  //       ),
  //     )
  //   }
  // }

  // public async genThumbnailUrl() {
  //   const bucket = this.getDataValue('bucket')

  //   if (/^image/.test(this.getDataValue('mimeType'))) {
  //     if (bucketMapping[bucket].isPublic) {
  //       this.setDataValue(
  //         'thumbnailUrl',
  //         bucketMapping[bucket].thumbnailBucket +
  //           this.getDataValue('path') +
  //           this.getDataValue('objectId'),
  //       )
  //     } else {
  //       const thumbnailUrl = await client.presignedUrl(
  //         'GET',
  //         bucketMapping[bucket].thumbnailBucket,
  //         this.getDataValue('path').substring(1) +
  //           this.getDataValue('objectId'),
  //         60,
  //       )
  //       this.setDataValue(
  //         'thumbnailUrl',
  //         thumbnailUrl.replace(
  //           new RegExp(
  //             `http://${configService.minio.server}:${configService.minio.port}`,
  //           ),
  //           '',
  //         ),
  //       )
  //     }
  //   }
  // }

  // public async genAll() {
  //   await this.genUrl()
  //   await this.genThumbnailUrl()
  // }

  public preSign(bucket: string, objectId: string) {
    const method = 'GET'
    const region = 'us-east-1'
    const expire = 5 * 60
    const reqOptions = client['getRequestOptions']({
      method,
      region,
      bucketName: bucket,
      objectName: objectId,
    })
    // presignSignatureV4(request, accessKey, secretKey, sessionToken, region, requestDate, expires)
    const presignedUrl = signing.presignSignatureV4(
      reqOptions,
      minioClientConfig.accessKey,
      minioClientConfig.secretKey,
      null,
      region,
      new Date(),
      expire,
    )
    return presignedUrl.replace(
      new RegExp(
        `http://${configService.minio.server}:${configService.minio.port}`,
      ),
      '',
    )
  }
}
