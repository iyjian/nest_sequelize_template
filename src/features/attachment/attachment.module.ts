import { Module } from '@nestjs/common'
import { AttachmentController } from './controllers'
import { AttachmentService } from './services'
import { Attachment } from './entities'
import { SequelizeModule } from '@nestjs/sequelize'

@Module({
  controllers: [AttachmentController],
  providers: [AttachmentService],
  imports: [SequelizeModule.forFeature([Attachment])],
  exports: [AttachmentService],
})
export class AttachmentModule {}
