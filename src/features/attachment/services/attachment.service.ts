import { Injectable, HttpException, HttpStatus, Logger } from '@nestjs/common'
import { InjectModel } from '@nestjs/sequelize'
import { Op, Transaction } from 'sequelize'
import { Sequelize } from 'sequelize-typescript'
import { BaseService } from './../../../core'
import { Attachment } from './../entities'
import {
  FindAllAttachmentRequestDTO,
  CreateAttachmentRequestDTO,
  UpdateAttachmentRequestDTO,
  FindOneAttachmentRequestDTO,
  UploadDTO,
} from './../dto'
import * as Minio from 'minio'
import { ConfigService } from '@nestjs/config'
import { nanoid } from 'nanoid'
import sharp from 'sharp'
import AWS from 'aws-sdk'
import { Stream } from 'stream'

@Injectable()
export class AttachmentService extends BaseService {
  private readonly logger = new Logger(AttachmentService.name)
  private readonly include: any[]
  private readonly client: Minio.Client
  private readonly minioClientConfig: Minio.ClientOptions
  private readonly minioHost = this.configService.get<string>('minio.server')
  private readonly minioPort = this.configService.get<number>('minio.port')
  private readonly bucketMapping: any

  constructor(
    @InjectModel(Attachment)
    private readonly attachmentModel: typeof Attachment,
    private readonly configService: ConfigService,
    private readonly mysql: Sequelize,
  ) {
    super()
    this.minioClientConfig = {
      accessKey: this.configService.get<string>('minio.accessKey'),
      secretKey: this.configService.get<string>('minio.secretKey'),
      endPoint: this.minioHost,
      port: this.minioPort,
      useSSL: false,
    }

    this.bucketMapping = {
      [this.configService.get<string>('minio.bucket')]:
        this.configService.get<string>('minio.thumbnailBucket'),
      [this.configService.get<string>('minio.publicBucket')]:
        this.configService.get<string>('minio.publicThumbnailBucket'),
    }

    this.client = new Minio.Client(this.minioClientConfig)
    this.include = []
  }

  saveToPublicBucketForDebug(objectId: string, buf: Buffer) {
    return this.client.putObject(
      this.configService.get<string>('minio.publicBucket'),
      `debug/${objectId}`,
      buf,
    )
  }

  async getPublicConfig() {
    return {
      token: {
        accessKey: '',
        secretKey: '',
        endPoint: this.minioHost,
        port: this.minioPort,
        useSSL: false,
      },
      stsToken: {
        publicEndpoint: this.configService.get<string>('minio.publicEndpoint'),
        endpoint: this.minioClientConfig.endPoint,
        port: this.minioClientConfig.port,
        accessKey: '',
        secretKey: '',
        sessionToken: '',
        expiration: '',
      },
      buckets: {
        privateBucket: this.configService.get<string>('minio.bucket'),
        publicBucket: this.configService.get<string>('minio.publicBucket'),
      },
    }
  }

  async getConfig() {
    const sts = new AWS.STS({
      apiVersion: '2012-10-17',
      endpoint: `http://${this.minioClientConfig.endPoint}:${this.minioClientConfig.port}`,
      accessKeyId: this.configService.get<string>('minio.accessKey'),
      secretAccessKey: this.configService.get<string>('minio.secretKey'),
      s3ForcePathStyle: true,
      region: 'us-east-1',
    })

    const params = {
      DurationSeconds: 1800,
      RoleArn: 'arn:*:*:*:*',
      RoleSessionName: 'temporarySession',
    }

    const stsToken = await new Promise<AWS.STS.AssumeRoleResponse>(
      (resolve, reject) => {
        sts.assumeRole(params, (err, data) => {
          if (err) {
            reject(err)
          } else {
            resolve(data)
          }
        })
      },
    )

    return {
      token: this.minioClientConfig,
      stsToken: {
        publicEndpoint: this.configService.get<string>('minio.publicEndpoint'),
        endpoint: this.minioClientConfig.endPoint,
        port: this.minioClientConfig.port,
        accessKey: stsToken.Credentials.AccessKeyId,
        secretKey: stsToken.Credentials.SecretAccessKey,
        sessionToken: stsToken.Credentials.SessionToken,
        expiration: stsToken.Credentials.Expiration,
      },
      buckets: {
        privateBucket: this.configService.get<string>('minio.bucket'),
        publicBucket: this.configService.get<string>('minio.publicBucket'),
      },
    }
  }

  async stream2buffer(stream: Stream): Promise<Buffer> {
    return new Promise<Buffer>((resolve, reject) => {
      const _buf = Array<any>()

      stream.on('data', (chunk) => _buf.push(chunk))
      stream.on('end', () => resolve(Buffer.concat(_buf)))
      stream.on('error', (err) => reject(`error converting stream - ${err}`))
    })
  }

  async upload(file: Express.Multer.File, body: UploadDTO) {
    const objectId = nanoid(32)

    await this.client.putObject(
      this.configService.get<string>('minio.publicBucket'),
      `${body.path}/${objectId}`,
      file.buffer,
      {
        'Content-Type': file.mimetype,
        thumbnail: /^image/.test(file.mimetype)
          ? this.bucketMapping[body.bucket]
          : '',
      },
    )

    // 如果是图片则放一份缩略图到缩略图bucket
    if (/^image/.test(file.mimetype)) {
      try {
        await this.client.putObject(
          this.bucketMapping[body.bucket],
          `${body.path}/${objectId}`,
          /^image\/svg/.test(file.mimetype)
            ? file.buffer
            : await sharp(file.buffer).resize(120).toBuffer(),
          {
            'Content-Type': file.mimetype,
          },
        )
      } catch (e) {
        console.log(e)
      }
    }

    const attachment = await this.createAttachment({
      bucket: body.bucket,
      objectId,
      path: body.path,
      objectName: body.objectName,
      mimeType: file.mimetype,
      size: file.size,
      sourceSystem: body.sourceSystem,
      isFolder: false,
    })
    return attachment
  }

  async createFolder(body: UploadDTO) {
    await this.client.putObject(body.bucket, `${body.path}`, '')

    const pathParts = body.path.split('/')
    const pathPartsLength = pathParts.length

    const attachment = await this.createAttachment({
      bucket: body.bucket,
      objectId: nanoid(),
      path: pathParts.slice(0, pathPartsLength - 2).join('/') + '/',
      objectName: pathParts[pathPartsLength - 2] + '/',
      mimeType: '',
      size: 0,
      sourceSystem: body.sourceSystem,
      isFolder: true,
    })

    return attachment
  }

  async createAttachment(attachmentRequest: CreateAttachmentRequestDTO) {
    return this.attachmentModel.create(attachmentRequest)
  }

  // TODO: 附件搜索需要建全文索引
  async findAll(findAllQueryAttachment: FindAllAttachmentRequestDTO) {
    const {
      page,
      pageSize,
      skipPaging,
      search,
      sort = [
        ['isFolder', 'desc'],
        ['createdAt', 'desc'],
      ],
      ...payload
    } = findAllQueryAttachment

    const condition = this.normalizeCondition(payload)

    if (search) {
      condition['objectName'] = {
        [Op.like]: `%${search}%`,
      }
    }

    /**
     * 限制文件类型的时候，需要同时查询文件夹
     */
    if (condition.mimeType) {
      condition[Op.or] = {
        mimeType: condition.mimeType,
        isFolder: condition.isFolder !== false,
      }
      delete condition.mimeType
    }

    const result = await this.attachmentModel.findAndCountAll({
      where: condition,
      offset: skipPaging ? undefined : (page - 1) * pageSize,
      limit: skipPaging ? undefined : pageSize,
      order: sort,
    })

    return result
  }

  async findByIds(ids: number[], transaction?: Transaction) {
    const attachments = await this.attachmentModel.findAll({
      where: {
        id: {
          [Op.in]: ids,
        },
      },
      transaction,
    })

    return attachments
  }

  async findOneByIdOrThrow(id: number, transaction?: Transaction) {
    const attachment = await this.attachmentModel
      .scope('findOne')
      .findByPk(id, {
        transaction,
      })

    if (!attachment) {
      throw new HttpException('未找到该记录', HttpStatus.NOT_FOUND)
    }

    return attachment
  }

  async findOneById(id: number, transaction?: Transaction) {
    const attachment = await this.attachmentModel
      .scope('findOne')
      .findByPk(id, {
        transaction,
      })

    return attachment
  }

  async findOneOrThrow(
    findOneAttachmentRequest: FindOneAttachmentRequestDTO,
    transaction?: Transaction,
  ) {
    const attachment = await this.attachmentModel.scope('findOne').findOne({
      where: { ...findOneAttachmentRequest },
      transaction,
    })

    if (!attachment) {
      throw new HttpException('未找到该记录', HttpStatus.NOT_FOUND)
    }

    return attachment
  }

  async findOne(
    findOneAttachmentRequest: FindOneAttachmentRequestDTO,
    transaction?: Transaction,
  ) {
    const attachment = await this.attachmentModel.scope('findOne').findOne({
      where: { ...findOneAttachmentRequest },
      transaction,
    })

    return attachment
  }

  async updateById(
    id: number,
    updateAttachmentRequest: UpdateAttachmentRequestDTO,
    transaction?: Transaction,
  ) {
    await this.attachmentModel.update(updateAttachmentRequest, {
      where: {
        id,
      },
      transaction,
    })
    return true
  }

  async removeById(id: number, transaction?: Transaction) {
    const isOuterTransaction = !!transaction
    try {
      if (!transaction) {
        transaction = await this.mysql.transaction()
      }

      await this.attachmentModel.update(
        { isActive: null },
        {
          where: {
            id,
          },
          transaction,
        },
      )

      if (!isOuterTransaction) {
        await transaction.commit()
      }
      return true
    } catch (e) {
      if (transaction && !isOuterTransaction) {
        await transaction.rollback()
        if (e.toString() === 'Error: SequelizeForeignKeyConstraintError') {
          throw new Error('有依赖数据，无法删除')
        }
      }
      throw e
    }
  }

  public async getObject(
    bucketName: string,
    objectName: string,
  ): Promise<Buffer> {
    return new Promise((resolve, reject) => {
      const chunks = []
      this.client.getObject(bucketName, objectName, (err, dataStream) => {
        if (err || !dataStream) {
          this.logger.debug(
            `getObject - bucketName: ${bucketName} objectName: ${objectName}`,
          )
          reject(new Error(err['message'] ?? '404'))
          return
        }
        dataStream.on('data', (chunk: Buffer) => {
          chunks.push(chunk)
        })
        dataStream.on('end', () => {
          resolve(Buffer.concat(chunks))
        })
        dataStream.on('error', (err) => {
          reject(err)
        })
      })
    })
  }
}
