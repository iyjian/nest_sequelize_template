import { ApiBody, ApiConsumes, ApiOperation, ApiTags } from '@nestjs/swagger'
import { CreateAttachmentRequestDTO, UploadDTO } from '../dto'
import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Query,
  UseInterceptors,
  UploadedFile,
  HttpException,
  HttpStatus,
} from '@nestjs/common'
import { FileInterceptor } from '@nestjs/platform-express'
import { AttachmentService } from '../services/attachment.service'
import {
  FindAllAttachmentRequestDTO,
  UpdateAttachmentRequestDTO,
} from './../dto'
import { Express } from 'express'
import {
  ApiFindOneResponse,
  ApiPaginatedResponse,
  ApiPatchResponse,
  codeGen,
} from '../../../core'

import {
  FindOneResponseSchema,
  FindAllResponseSchema,
} from '../dto/attachment.response.schema'

@Controller('attachment')
@ApiTags('记录附件(文件、图片)的表')
export class AttachmentController {
  constructor(private readonly attachmentService: AttachmentService) {}

  @Get('config/public')
  @ApiOperation({
    summary: '获取附件的公网配置',
  })
  async getPublicConfig() {
    return this.attachmentService.getPublicConfig()
  }

  @Get('config')
  @ApiOperation({
    summary: '获取上传附件的配置',
  })
  async getConfig() {
    return this.attachmentService.getConfig()
  }

  /**
   * 文件上传接口
   * path path结尾需要有 /
   */
  @Post('upload')
  @ApiOperation({
    summary: '上传附件',
  })
  @ApiConsumes('multipart/form-data')
  @ApiFindOneResponse('attachment', FindOneResponseSchema)
  @UseInterceptors(FileInterceptor('file'))
  @ApiBody({
    description: 'upload tide data',
    type: UploadDTO,
  })
  async uploadFile(
    @UploadedFile() file: Express.Multer.File,
    @Body() body: UploadDTO,
  ) {
    return this.attachmentService.upload(file, body)
  }

  @Post('')
  @ApiOperation({
    summary: '新增附件',
  })
  @codeGen('548-create')
  @ApiFindOneResponse('attachment', FindOneResponseSchema)
  create(@Body() createAttachmentRequest: CreateAttachmentRequestDTO) {
    return this.attachmentService.createAttachment(createAttachmentRequest)
  }

  /**
   * 创建目录
   */
  @Post('path')
  @ApiOperation({
    summary: '创建目录',
  })
  @ApiFindOneResponse('attachment', FindOneResponseSchema)
  async createFolder(@Body() body: UploadDTO) {
    if (body.path.substring(body.path.length - 1) !== '/') {
      throw new HttpException('路径必须以/结尾', HttpStatus.BAD_REQUEST)
    }
    if (body.path === '/') {
      throw new HttpException('无法创建根目录', HttpStatus.BAD_REQUEST)
    }

    return this.attachmentService.createFolder(body)
  }

  @Patch(':id')
  @ApiOperation({
    summary: 'PATCH attachment',
  })
  @ApiPatchResponse('attachment')
  @codeGen('548-update')
  update(
    @Param('id') id: string,
    @Body() updateAttachmentDto: UpdateAttachmentRequestDTO,
  ) {
    return this.attachmentService.updateById(+id, updateAttachmentDto)
  }

  @Get('')
  @ApiOperation({
    summary: 'GET attachment(list)',
  })
  @ApiPaginatedResponse('attachment', FindAllResponseSchema)
  @codeGen('548-findAll')
  findAll(@Query() findAllQueryAttachment: FindAllAttachmentRequestDTO) {
    return this.attachmentService.findAll(findAllQueryAttachment)
  }
}
