import { PagingRequestDTO, getTransformer, codeGen } from '../../../core'
import { Transform, Type } from 'class-transformer'
import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty } from 'class-validator'

export class UploadDTO {
  @ApiProperty({ description: '上传附件', type: 'string', format: 'binary' })
  file: any

  @ApiProperty({
    description: '文件路径',
    required: false,
  })
  path: string

  @ApiProperty({
    description: '上传的目标桶',
    required: false,
  })
  bucket: string

  @ApiProperty({
    description: '对象名称',
    required: false,
  })
  objectName: string

  @ApiProperty({
    description: '来源系统',
    required: false,
  })
  sourceSystem: string
}

export class CreateAttachmentRequestDTO {
  @codeGen('7776')
  @ApiProperty({
    description: '文件对象id',
    required: false,
  })
  objectId?: string

  @codeGen('7777')
  @ApiProperty({
    description: '文件名',
    required: false,
  })
  objectName?: string

  @codeGen('7778')
  @ApiProperty({
    description: ' 文件类型',
    required: false,
  })
  mimeType?: string

  @codeGen('7779')
  @ApiProperty({
    description: '备注',
    required: false,
  })
  remark?: string

  @codeGen('7780')
  @ApiProperty({
    description: '来源系统',
    required: false,
  })
  sourceSystem?: string

  @codeGen('7781')
  @ApiProperty({
    description: '文件路径',
    required: false,
  })
  path?: string

  @codeGen('7782')
  @ApiProperty({
    description: '桶',
    required: false,
  })
  bucket?: string

  @codeGen('7783')
  @ApiProperty({
    description: '是否是文件夹',
    required: false,
  })
  @Transform(getTransformer('booleanTransformer'))
  isFolder?: boolean

  @codeGen('7784')
  @ApiProperty({
    description: '文件大小',
    required: false,
  })
  @Transform(getTransformer('numberTransformer'))
  size?: number
}

export class UpdateAttachmentRequestDTO {
  @codeGen('7776')
  @ApiProperty({
    description: '文件对象id',
    required: false,
  })
  objectId?: string

  @codeGen('7777')
  @ApiProperty({
    description: '文件名',
    required: false,
  })
  objectName?: string

  @codeGen('7778')
  @ApiProperty({
    description: ' 文件类型',
    required: false,
  })
  mimeType?: string

  @codeGen('7779')
  @ApiProperty({
    description: '备注',
    required: false,
  })
  remark?: string

  @codeGen('7780')
  @ApiProperty({
    description: '来源系统',
    required: false,
  })
  sourceSystem?: string

  @codeGen('7781')
  @ApiProperty({
    description: '文件路径',
    required: false,
  })
  path?: string

  @codeGen('7782')
  @ApiProperty({
    description: '桶',
    required: false,
  })
  bucket?: string

  @codeGen('7783')
  @ApiProperty({
    description: '是否是文件夹',
    required: false,
  })
  @Transform(getTransformer('booleanTransformer'))
  isFolder?: boolean

  @codeGen('7784')
  @ApiProperty({
    description: '文件大小',
    required: false,
  })
  @Transform(getTransformer('numberTransformer'))
  size?: number
}

export class FindAllAttachmentRequestDTO extends PagingRequestDTO {
  @codeGen('7776')
  @ApiProperty({
    description: '文件对象id',
    required: false,
  })
  objectId?: string

  @codeGen('7777')
  @ApiProperty({
    description: '文件名',
    required: false,
  })
  objectName?: string

  @codeGen('7778')
  @ApiProperty({
    description: ' 文件类型',
    required: false,
  })
  mimeType?: string

  @codeGen('7779')
  @ApiProperty({
    description: '备注',
    required: false,
  })
  remark?: string

  @codeGen('7780')
  @ApiProperty({
    description: '来源系统',
    required: false,
  })
  sourceSystem?: string

  @codeGen('7781')
  @ApiProperty({
    description: '文件路径',
    required: false,
  })
  path?: string

  @codeGen('7782')
  @ApiProperty({
    description: '桶',
    required: false,
  })
  bucket?: string

  @codeGen('7783')
  @ApiProperty({
    description: '是否是文件夹',
    required: false,
  })
  @Transform(getTransformer('booleanTransformer'))
  isFolder?: boolean

  @codeGen('7784')
  @ApiProperty({
    description: '文件大小',
    required: false,
  })
  @Transform(getTransformer('numberTransformer'))
  size?: number
}

export class FindOneAttachmentRequestDTO {
  @codeGen('7776')
  @ApiProperty({
    description: '文件对象id',
    required: false,
  })
  objectId?: string

  @codeGen('7777')
  @ApiProperty({
    description: '文件名',
    required: false,
  })
  objectName?: string

  @codeGen('7778')
  @ApiProperty({
    description: ' 文件类型',
    required: false,
  })
  mimeType?: string

  @codeGen('7779')
  @ApiProperty({
    description: '备注',
    required: false,
  })
  remark?: string

  @codeGen('7780')
  @ApiProperty({
    description: '来源系统',
    required: false,
  })
  sourceSystem?: string

  @codeGen('7781')
  @ApiProperty({
    description: '文件路径',
    required: false,
  })
  path?: string

  @codeGen('7782')
  @ApiProperty({
    description: '桶',
    required: false,
  })
  bucket?: string

  @codeGen('7783')
  @ApiProperty({
    description: '是否是文件夹',
    required: false,
  })
  @Transform(getTransformer('booleanTransformer'))
  isFolder?: boolean

  @codeGen('7784')
  @ApiProperty({
    description: '文件大小',
    required: false,
  })
  @Transform(getTransformer('numberTransformer'))
  size?: number
}
