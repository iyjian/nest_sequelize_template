export const FindOneResponseSchema = {
  objectId: { type: 'string', example: '', description: '文件对象id' },
  objectName: { type: 'string', example: '', description: '文件名' },
  mimeType: { type: 'string', example: '', description: ' 文件类型' },
  remark: { type: 'string', example: '', description: '备注' },
  sourceSystem: { type: 'string', example: '', description: '来源系统' },
  path: { type: 'string', example: '', description: '文件路径' },
  bucket: { type: 'string', example: '', description: '桶' },
  isFolder: { type: 'boolean', example: '', description: '是否是文件夹' },
  size: { type: 'number', example: '', description: '文件大小' },
}

export const FindAllResponseSchema = {
  objectId: { type: 'string', example: '', description: '文件对象id' },
  objectName: { type: 'string', example: '', description: '文件名' },
  mimeType: { type: 'string', example: '', description: ' 文件类型' },
  remark: { type: 'string', example: '', description: '备注' },
  sourceSystem: { type: 'string', example: '', description: '来源系统' },
  path: { type: 'string', example: '', description: '文件路径' },
  bucket: { type: 'string', example: '', description: '桶' },
  isFolder: { type: 'boolean', example: '', description: '是否是文件夹' },
  size: { type: 'number', example: '', description: '文件大小' },
}
