import { Injectable } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { AmqpConnection } from '@golevelup/nestjs-rabbitmq'
import { ExchangeConfig } from './../../config'
import { HTTP_REQUEST_LOG_TYPE, HTTP_RESPONSE_LOG_TYPE } from './types'

export type SendMailRequest = {
  to: string
  subject: string
  content: string
}

@Injectable()
export class AsyncClientService {
  public constructor(
    private readonly configService: ConfigService,
    private readonly amqpConnection: AmqpConnection,
  ) {}

  public async RPC(msg: any) {
    return this.amqpConnection.request({
      exchange:
        this.configService.get<ExchangeConfig>('asyncTaskExchange').exchange,
      routingKey: 'rpc',
      payload: msg,
      timeout: 10000,
    })
  }

  public saveScanLog(msg: any) {
    return this.amqpConnection.publish(
      this.configService.get<ExchangeConfig>('asyncTaskExchange').exchange,
      'task.scanlog',
      msg,
    )
  }

  /**
   * 发送邮件
   * @param msg
   */
  public async sendMail(msg: SendMailRequest) {
    this.amqpConnection.publish(
      this.configService.get<ExchangeConfig>('asyncTaskExchange').exchange,
      'mail',
      msg,
    )
  }

  /**
   * 发送日志
   * @param msg
   */
  public async sendLog(msg: HTTP_REQUEST_LOG_TYPE) {
    const { id, requestDate, userId, method, path, url, ip, payload } = msg

    this.amqpConnection.publish(
      this.configService.get<ExchangeConfig>('loggingExchange').exchange,
      'logging',
      {
        id,
        requestDate,
        userId,
        method,
        path,
        url,
        ip,
        payload,
      },
    )
  }

  public async updateLog(msg: HTTP_RESPONSE_LOG_TYPE) {
    const { requestId, duration, statusCode } = msg
    this.amqpConnection.publish(
      this.configService.get<ExchangeConfig>('loggingExchange').exchange,
      'logging',
      {
        type: 'update',
        requestId,
        duration,
        statusCode,
      },
    )
  }
}
