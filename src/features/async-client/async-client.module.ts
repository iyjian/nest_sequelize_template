import { RabbitMQModule } from '@golevelup/nestjs-rabbitmq'
import { Module } from '@nestjs/common'
import { AsyncClientService } from './async-client.service'
import { ExchangeConfig, RabbitConfig } from './../../config'
import { ConfigModule, ConfigService } from '@nestjs/config'

@Module({
  imports: [
    RabbitMQModule.forRootAsync(RabbitMQModule, {
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        exchanges: [
          {
            name: configService.get<ExchangeConfig>('loggingExchange').exchange,
            type: 'topic',
          },
          {
            name: configService.get<ExchangeConfig>('asyncTaskExchange')
              .exchange,
            type: 'topic',
          },
        ],
        channels: {
          sequential: {
            prefetchCount: 1,
            default: true,
          },
        },
        uri: configService.get<RabbitConfig>('rabbit').uri,
        enableControllerDiscovery: true,
        connectionInitOptions: { wait: false },
      }),
      inject: [ConfigService],
    }),
  ],
  controllers: [],
  providers: [AsyncClientService],
  exports: [AsyncClientService],
})
export class AsyncClientModule {}
