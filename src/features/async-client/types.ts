export type HTTP_REQUEST_LOG_TYPE = {
  id: string
  requestDate: string
  userId: number
  method: string
  path: string
  url: string
  ip: string
  payload: any
  routeId?: number
  requestDateTimestamp?: number
}

export type HTTP_RESPONSE_LOG_TYPE = {
  type?: string
  requestId: string
  duration: number
  statusCode: number
}
