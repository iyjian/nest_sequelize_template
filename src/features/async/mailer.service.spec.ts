import { ConfigModule, ConfigService } from '@nestjs/config'
import { Test } from '@nestjs/testing'
import configuration from './../../config/configuration'
import { MailerService } from './mailer.service'

describe('MailerService Test', () => {
  let mailerService: MailerService

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot({
          load: [configuration],
          isGlobal: true,
        }),
      ],
      providers: [MailerService],
    }).compile()
    mailerService = moduleRef.get(MailerService)
  })

  describe('send mail test - text message & single recipient', () => {
    it('should send a text mail', async () => {
      const result = await mailerService.send({
        to: 'wc@bigcruise.cn',
        subject: 'thank you',
        content: 'hello world!',
      })
      expect(result.accepted).toEqual(['wc@bigcruise.cn'])
    })

    it('should send a html mail', async () => {
      const result = await mailerService.send({
        to: 'wc@bigcruise.cn',
        subject: 'thank you',
        content: 'hello world!<br/>i am here!',
      })
      expect(result.accepted).toEqual(['wc@bigcruise.cn'])
    })
  })
})
