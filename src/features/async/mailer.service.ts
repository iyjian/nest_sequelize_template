import nodemailer from 'nodemailer'
import { Injectable } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'

@Injectable()
export class MailerService {
  private readonly transporter: nodemailer.Transporter

  public constructor(private readonly configService: ConfigService) {
    const host = this.configService.get('mailer.server')
    const port = this.configService.get('mailer.port')
    const user = this.configService.get('mailer.userName')
    const pass = this.configService.get('mailer.passwd')
    this.transporter = nodemailer.createTransport({
      secure: true,
      port,
      host,
      auth: {
        user,
        pass,
      },
      tls: {
        rejectUnauthorized: false,
      },
    })
  }

  /**
   * 发送邮件
   * @param msg
   */
  public async send(msg: { to: string; subject: string; content: string }) {
    return this.transporter.sendMail({
      from: this.configService.get('mailer.sender'),
      to: msg.to,
      subject: msg.subject,
      html: msg.content,
    })
  }
}
