import { Injectable, Logger } from '@nestjs/common'
import { LogService, Route, RouteService } from '../base'
import 'dotenv/config'
import { RabbitRPC } from '@golevelup/nestjs-rabbitmq'
import ConfigService from './../../config/configuration'
import {
  HTTP_REQUEST_LOG_TYPE,
  HTTP_RESPONSE_LOG_TYPE,
} from '../async-client/types'
import moment from 'moment'

const configService = ConfigService()

@Injectable()
export class AsyncService {
  private routes: Route[]
  private isInit = false
  private readonly logger = new Logger(AsyncService.name)

  constructor(
    private readonly logService: LogService,
    private readonly routeService: RouteService,
  ) {}

  public async saveLog(msg: HTTP_REQUEST_LOG_TYPE & HTTP_RESPONSE_LOG_TYPE) {
    if (msg.type === 'update') {
      await this.logService.updateByRequestId(msg.requestId, msg)
    } else {
      if ((!this.routes || this.routes.length === 0) && !this.isInit) {
        /**
         * 如果没有加载过routes则加载到内存
         */
        this.isInit = true
        this.routes = (
          await this.routeService.findAll({ skipPaging: true })
        ).rows
      }

      const routes =
        this.routes && this.routes.length > 0
          ? this.routes
          : (await this.routeService.findAll({ skipPaging: true })).rows

      for (const route of routes) {
        if (msg.method.toUpperCase() === route.method.toUpperCase()) {
          /**
           * 如果匹配到路由，则先添加路由编号(routeId)到日志信息中，然后再存储日志
           */
          const m = route.regexp.match(/\/(.*)\/(.*)?/)
          if (new RegExp(m[1], m[2] || '').test(msg.path)) {
            msg.routeId = route.id
            break
          }
        }
      }

      msg.payload = JSON.stringify(msg.payload)
      // msg.requestId = msg.id
      await this.logService.create(msg)
    }
  }

  @RabbitRPC({
    exchange: configService.asyncTaskExchange.exchange,
    queue: configService.asyncTaskExchange.queue + '_rpc',
    routingKey: 'rpc',
  })
  public async RPC(msg: any) {
    //
  }
}
