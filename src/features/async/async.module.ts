import { Module } from '@nestjs/common'
import { AsyncService } from './async.service'
import { BaseModule } from '../base'
import { MailerService } from './mailer.service'
import { AmqpConnection, RabbitMQModule } from '@golevelup/nestjs-rabbitmq'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { AsyncWorkerController } from './async.worker.controller'
import { ExchangeConfig } from './../../config'

@Module({
  imports: [
    BaseModule,
    RabbitMQModule.forRootAsync(RabbitMQModule, {
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        exchanges: [
          {
            name: configService.get<ExchangeConfig>('loggingExchange').exchange,
            type: 'topic',
          },
          {
            name: configService.get<ExchangeConfig>('asyncTaskExchange')
              .exchange,
            type: 'topic',
          },
          {
            name: configService.get<ExchangeConfig>('loggingExchange').dlx,
            type: 'topic',
          },
          {
            name: configService.get<ExchangeConfig>('asyncTaskExchange').dlx,
            type: 'topic',
          },
        ],
        channels: {
          oneByOne: {
            prefetchCount: 1,
            default: true,
          },
        },
        uri: `amqp://${configService.get('rabbit.user')}:${configService.get(
          'rabbit.passwd',
        )}@${configService.get('rabbit.host')}:${configService.get(
          'rabbit.port',
        )}`,
        enableControllerDiscovery: true,
        connectionInitOptions: { wait: true },
      }),
      inject: [ConfigService],
    }),
  ],
  providers: [AsyncService, MailerService],
  controllers: [AsyncWorkerController],
  exports: [AsyncService],
})
export class AsyncModule {
  constructor(
    private readonly amqpConnection: AmqpConnection,
    private readonly configService: ConfigService,
  ) {
    // 创建队列
    this.amqpConnection.channel.assertQueue(
      this.configService.get<ExchangeConfig>('loggingExchange').dlQueue,
    )

    this.amqpConnection.channel.assertQueue(
      this.configService.get<ExchangeConfig>('asyncTaskExchange').dlQueue,
    )

    // 绑定queue和exchange
    this.amqpConnection.channel.bindQueue(
      this.configService.get<ExchangeConfig>('loggingExchange').dlQueue,
      this.configService.get<ExchangeConfig>('loggingExchange').dlx,
      '*',
    )

    this.amqpConnection.channel.bindQueue(
      this.configService.get<ExchangeConfig>('asyncTaskExchange').dlQueue,
      this.configService.get<ExchangeConfig>('asyncTaskExchange').dlx,
      '*',
    )
  }
}
