export type SendMailRequest = {
  to: string
  subject: string
  content: string
}
