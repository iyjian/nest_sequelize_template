import { RabbitSubscribe, Nack } from '@golevelup/nestjs-rabbitmq'
import { Controller, Logger } from '@nestjs/common'
import { AsyncService } from './async.service'
import { MailerService } from './mailer.service'
import 'dotenv/config'
// import {
//   getExchangeName,
//   getQueueName,
//   getDeadLetterExchangeName,
// } from './../../config'
import { SendMailRequest } from './dto/request.dto'
import ConfigService from './../../config/configuration'

const configService = ConfigService()

console.log(configService.asyncTaskExchange)
@Controller('async/worker')
export class AsyncWorkerController {
  private readonly logger = new Logger(AsyncWorkerController.name)

  constructor(
    private readonly asyncService: AsyncService,
    private readonly mailerService: MailerService,
  ) {}

  /**
   * 异步记录日志
   *
   * @param msg
   * @returns
   */
  @RabbitSubscribe({
    exchange: configService.loggingExchange.exchange,
    routingKey: 'logging',
    queue: configService.loggingExchange.queue,
    queueOptions: {
      deadLetterExchange: configService.loggingExchange.dlx,
    },
  })
  public async saveLog(msg: any) {
    try {
      this.logger.debug(`logging - savelog - success`)
      await this.asyncService.saveLog(msg)
    } catch (e) {
      this.logger.error(`logging - savelog - failed to save log`)
      this.logger.error(JSON.stringify(msg, null, 2))
      this.logger.error(e)
      this.logger.error(`logging - savelog - failed to save log`)
      return new Nack()
    }
  }

  /**
   * 异步发送邮件
   *
   * @param msg
   * @returns
   */
  @RabbitSubscribe({
    exchange: configService.asyncTaskExchange.exchange,
    queue: configService.asyncTaskExchange.queue + '_mail',
    routingKey: 'mail',
    queueOptions: {
      deadLetterExchange: configService.asyncTaskExchange.dlx,
    },
  })
  public async mail(msg: SendMailRequest) {
    try {
      await this.mailerService.send(msg)
    } catch (e) {
      return new Nack()
    }
  }
}
