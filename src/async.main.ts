import { NestFactory } from '@nestjs/core'
import { CacheModule, Module } from '@nestjs/common'
import configuration from './config/configuration'
import redisStore from 'cache-manager-ioredis'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { SequelizeModule } from '@nestjs/sequelize'
import { BaseModule } from './features/base'
import { AsyncModule } from './features/async'
import { MongooseModule } from '@nestjs/mongoose'

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [configuration],
      isGlobal: true,
    }),
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        uri: configService.get('mongo.loggingDBURI'),
      }),
      inject: [ConfigService],
    }),
    SequelizeModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => {
        return {
          dialect: 'mysql',
          host: configService.get('mysql.host'),
          port: +configService.get<number>('mysql.port'),
          username: configService.get('mysql.username'),
          password: configService.get('mysql.password'),
          database: configService.get('mysql.db'),
          models: [],
          autoLoadModels: true,
          synchronize: true,
          sync: {
            alter: false,
          },
          pool: {
            max: 100,
            min: 0,
            idle: 10000,
          },
          timezone: '+08:00',
          logging:
            configService.get<string>('sqlLogging') === 'true' ? true : false,
        }
      },
      inject: [ConfigService],
    }),
    CacheModule.registerAsync({
      useFactory: (configService: ConfigService) => ({
        store: redisStore,
        host: configService.get('redis.host'),
        port: +configService.get<number>('redis.port'),
        password: configService.get('redis.password'),
        db: +configService.get<number>('redis.db'),
        ttl: 0,
      }),
      inject: [ConfigService],
      isGlobal: true,
    }),
    BaseModule,
    AsyncModule,
  ],
  providers: [],
})
export class AppModule {}

async function bootstrap() {
  await NestFactory.createApplicationContext(AppModule)
}

bootstrap()
