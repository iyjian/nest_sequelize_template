import 'dotenv/config'
import MeiliSearch from 'meilisearch'
;(async () => {
  const client = new MeiliSearch({
    host: process.env.MEILISEARCH_ENDPOINT,
    apiKey: process.env.MEILISEARCH_MASTER_KEY,
  })
  // await client.deleteIndex('log')
  // await client.deleteIndex('article')

  // await client.createIndex('log', {
  //   primaryKey: 'id',
  // })

  await client.index('log').updateSettings({
    filterableAttributes: [
      'id',
      'requestDate',
      'method',
      'routeId',
      'statusCode',
      'ip',
      'requestDateTimestamp',
    ],
    sortableAttributes: [
      'id',
      'requestDate',
      'duration',
      'requestDateTimestamp',
    ],
  })
  console.log('log index created')
})()
