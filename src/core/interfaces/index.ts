export * from './request.dto'
export * from './response.dto'

export interface USER {
  id: number
  isAdmin: boolean
}
