import { createParamDecorator, ExecutionContext } from '@nestjs/common'

export const ReqEmployeeId = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    // const request = ctx.switchToHttp().getRequest<Request>()
    return 1
  },
)

export const ReqUserId = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest<Request>() as any
    if (request?.locals?.user?.id) {
      return request['locals']['user']['id']
    } else {
      return 1
    }
  },
)

export const ReqUser = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest<Request>() as any
    if (request?.locals?.user?.id) {
      return request['locals']['user']
    } else {
      return {
        id: 1,
      }
    }
  },
)
