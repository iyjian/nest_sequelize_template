import {
  HttpException,
  HttpStatus,
  Injectable,
  Logger,
  NestMiddleware,
} from '@nestjs/common'
import { Response, NextFunction } from 'express'
import axios from 'axios'
import { ConfigService } from '@nestjs/config'
import { AuthingRequest } from '../interfaces'
import { nanoid } from 'nanoid'
import { AsyncClientService } from './../../features/async-client'

@Injectable()
export class AuthMiddleware implements NestMiddleware {
  logger = new Logger(AuthMiddleware.name)
  constructor(
    private readonly configService: ConfigService,
    private readonly asyncClientService: AsyncClientService,
  ) {}

  async use(
    req: AuthingRequest,
    _res: Response,
    next: NextFunction,
  ): Promise<void> {
    const startTime = process.hrtime()
    const requestId = nanoid(32)
    req.headers['requestId'] = requestId
    /**
     * token有两个模式
     * 1. 标准模式: 通过cookie传入
     * 2. 作弊模式: 前端通过query参数传入,比如导出文件链接
     */
    const token = req.get('token') || req?.query?.token?.toString()
    const appId = req.get('appId')
    const nonce = req.get('nonce')
    const sign = req.get('sign')
    const timestamp = req.get('timestamp')

    delete req.query.token

    if (/^\/i18n/.test(req.path) || /swagger/.test(req.path)) {
      /**
       * public接口，无需认证
       */
      next()
    } else if (this.configService.get<string>('NODE_ENV') === 'development') {
      /**
       * 开发环境无需认证
       */
      req.locals = {
        user: { id: 1 },
        token: token || '0f226e908d094e77ab32d096e49cd896',
      }
      next()
    } else if (token || (appId && nonce && sign)) {
      /**
       * 需要认证的接口，调用authing权限检查接口检查权限
       */
      try {
        const response = await axios.post(
          `${this.configService.get<string>(
            'auth.server',
          )}/auth/permissions/check`,
          {
            path: req.path,
            action: req.method.toLowerCase(),
          },
          {
            headers: {
              token,
              appId,
              nonce,
              timestamp,
              sign,
            },
          },
        )

        if (response.data.err !== 0) {
          throw new HttpException(response.data.errMsg, response.data.err)
        }

        if (!response.data.data.permission) {
          throw new HttpException('无权限', HttpStatus.FORBIDDEN)
        }

        req.locals = {
          user: response.data.data.user,
          token,
        }

        this.logger.verbose(
          `set user info in req.locals user: ${JSON.stringify(
            req['locals']['user'],
          )}`,
        )

        next()
      } catch (e) {
        throw e
      }
    } else {
      throw new HttpException('无效TOKEN,请重新登录', HttpStatus.UNAUTHORIZED)
    }

    _res.on('finish', () => {
      const { statusCode } = _res
      const diff = process.hrtime(startTime)
      const duration = diff[0] * 1e3 + diff[1] * 1e-6
      this.asyncClientService.updateLog({
        requestId,
        duration,
        statusCode,
      })
    })
  }
}
