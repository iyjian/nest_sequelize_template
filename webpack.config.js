const path = require('path')
const nodeExternals = require('webpack-node-externals');

module.exports = {
  mode: 'none',
  entry: {
    main: path.join(__dirname, './src/main.ts'),
    asyncmain: path.join(__dirname, './src/async.main.ts'),
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].js',
  },
  resolve: {
    // Add `.ts` and `.tsx` as a resolvable extension.
    extensions: ['.ts', '.tsx', '.js'],
  },
  module: {
    rules: [
      // all files with a `.ts` or `.tsx` extension will be handled by `ts-loader`
      { test: /\.tsx?$/, loader: 'ts-loader' },
    ],
  },
  externals: [
    nodeExternals({
      allowlist: [/axios/i],
    }),
  ]
}
